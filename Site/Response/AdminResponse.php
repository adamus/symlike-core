<?php
namespace Core\Site\Response;

use Core\Site\Response\DefaultResponse as BaseResponse;

class AdminResponse extends BaseResponse
{
    public function index()
    {
        $this->setup = $this->app->getSetup()->toArray();
        $this->display('Core:Site:Admin:index.html.twig');
    }

    public function repo()
    {
        $this->addMessage( nl2br(shell_exec("app/update.sh 2>&1")) );
        $this->clearCache();
    }

    public function schemaUpdate()
    {
        if( $this->router->getRequestMethod() == 'POST' ) {
            $confirm = $this->router->getParameter( 'confirm', false );
            $password = $this->router->getParameter( 'password', null );
            if( !$this->client->testPass( $password ) ) {
                return $this->redirect( $this->app->getRouter()->generate( 'logout' ) );
            }

            $this->schema_update = \App::getSchema()->schemaUpdate();
        }

        $this->index();
    }

    public function setup( $options = array(), $password = null )
    {
        if( $this->router->getRequestMethod() == 'POST' ) {
            if( !$this->client->testPass( $password ) ) {
                return $this->redirect( $this->app->getRouter()->generate( 'logout' ) );
            }

            if( $options['new_option_name'] != '' && $options['new_option_value'] != '' ) {
                $options[ $options['new_option_name'] ] = $options['new_option_value'];
            }
            unset( $options['new_option_name'] );
            unset( $options['new_option_value'] );

            $setup_collector = $this->app->getSetup();
            $setups = $setup_collector->toArray('name');

            foreach( $options as $option_name => $option_value ) {
                if( isset($setups[$option_name])  && $setups[$option_name]['value'] != $option_value && $this->client->testFlag( $setups[$option_name]['flag'] ) ) {
                    $setups[$option_name]['value'] = $option_value;
                    $setup_collector->updateRowByKey( 'name', $option_name, (object)$setups[$option_name] );
                } elseif( !isset($setups[$option_name]) ) {
                    $setup_collector->addRow((object)array(
                            'name' => $option_name,
                            'value' => $option_value,
                            'flag' => 1
                    ));
                }
            }
        }

        $this->reload();
    }

    public function cache()
    {
        $router = \App::getRouter();
        if( $router->getRequestMethod() == 'POST' && $router->getParameter( 'confirm' , 'false') == 'true' ) {
            \App::clearCache(true);
        }

        $this->cache_size = \Core\Kernel\Driver\CacheController::cacheSize();

        $this->display( 'Core:Site:Admin:cache.html.twig' );
    }

    public function logs()
    {
        $this->selected_log = \App::getRouter()->getParameter( 'log' , 'frontend.txt');
        $logs_root = \App::getDir( 'logs_root');

        $this->logs = array();
        if ( @is_dir($logs_root) && $dirhandle = @opendir($logs_root) ) {
            while( false !== ($filename = readdir($dirhandle)) ) {
                if( substr($filename, 0, 1) ==  "." ) continue;
                $this->logs[] = $filename;
            }
        }

        $this->current_log = "";
        if( in_array($this->selected_log, $this->logs) ) {
            $this->current_log = file_get_contents( $logs_root . DS . $this->selected_log );
        }

        $this->display( 'Core:Site:Admin:logs.html.twig' );
    }
}
