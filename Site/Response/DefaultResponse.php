<?php
namespace Core\Site\Response;

/**
 * A twig sablonokat lehívó és a hozzájuk tartozó fordításokat betöltő alap osztály.
 *
 * @author Török Ádám (adamus.tork@gmail.com)
 */
class DefaultResponse
{

    protected $_headers = array("Content-Type" => "text/html; charset=utf-8");

    /**
     * @var \App $app
     */
    public $app = null;

    /**
     * @var \Core\Site\Model\UserBase $client
     */
    public $client = null;

    /**
     * @var \Core\Site\Driver\Router $router
     */
    public $router = null;

    protected $client_class = '';

    function __construct() {
        $this->app = new \App();
        $this->client = $this->app->getClient();
        $this->client_class = \App::getClientClass();
        $this->router = \App::getRouter();
    }

    protected function sendHeaders()
    {
        foreach( $this->_headers as $key=>$value ) {
            header("{$key}: {$value}");
        }
    }

    /**
     * Megjeleníti a sablont.
     * @param string $template a sablon bázisrelatív helye.
     */
    protected function display( $template, $args=array() )
    {
        if( !$this->router->isPOST() && !in_array($this->router->getCurrentRoute(), array('not-found', 'not-allowed', 'asset-generator')) && $this->_headers['Content-Type'] == "text/html; charset=utf-8" ) {
            $this->app->getSession()->lastroute = $this->router->getCurrentRequest();
        }
        $this->sendHeaders();
        echo $this->render($template);
    }

    protected function render( $template, $args=array() )
    {
        if( !is_array($args) ) $args=array();
        return \App::getTwig()->render($template, array_merge((array)$this, $args) );
    }

    public function assetGenerator( $path, $mime )
    {
        $req_route = explode( '/', $path );
        $req_len = count($req_route);
        $tmpl_route = "";
        for( $i=0; $i < $req_len-1; $i++ )
            $tmpl_route .= ucfirst( $req_route[$i] ) . ':';

        $tmpl_route .= 'Asset:'  . $req_route[$req_len-1] . ".{$mime}";

        $this->setHeaderType( $mime );
        $this->display( $tmpl_route );
    }

	/**
	 * Főoldal
	 */
	function index()
	{
		$this->display('Core:Site:index.html.twig');
	}

	function redirect($destination)
	{
		if (!\App::SITE_DEBUG) {
			header("Refresh: 0; url=".curServer().$destination);
		}
		$this->redirect=$destination;
		$this->display('Core:Site:redirect.html.twig');
	}

	/**
	 * Lap újratöltése.
	 */
	function reload()
	{
		$current_route = \App::getRouter()->request();
		$last_route = \App::getSession()->lastroute;
		$redirect_to = \App::getRouter()->preindex() . (!empty($last_route)&&$last_route!=$current_route?$last_route:'/');

		$this->redirect($redirect_to);
	}

    protected function addMessage( $message, $translation = 'CoreSite:base', $class = 'error', $paramteres = array() )
    {
        $this->app->getSession()->addMessage($message, $translation, $class, $paramteres);
    }

	/**
	 * Gyorsítótár törlése és a lap újratöltése.
	 */
	function clearCache($redirection = '') {
		\App::clearCache();

		if (empty($redirection)) {
			$this->reload();
		} else {
			$this->redirect($redirection);
		}
	}

	function notFound($message='', $message_params = array(), $trans_domain='CoreSite:base') {
		$this->message = $message;
		$this->message_params = $message_params;
		$this->trans_domain = $trans_domain;
		$this->display('Core:Site:notfound.html.twig');
	}
	function notAllowed() {
		header('HTTP/1.1 403 Forbidden');
		$this->display('Core:Site:notallowed.html.twig');
	}

	protected function setHeaderType( $type )
	{
	    switch( $type ) {
	        case 'js':
	            $this->_headers['Content-Type'] = "text/javascript; charset=utf-8";
	            break;
	        case 'json':
	            $this->_headers['Expires'] = "Mon, 26 Jul 1997 05:00:00 GMT";
	            $this->_headers['Last-Modified'] = gmdate( "D, d M Y H:i:s" ) . "GMT";
	            $this->_headers['Cache-Control'] = "no-cache, must-revalidate";
	            $this->_headers['Pragma'] = "no-cache";
	            $this->_headers['Content-Type'] = "application/json; charset=utf-8";
	            break;
	    }
	}

}