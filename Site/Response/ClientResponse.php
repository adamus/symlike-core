<?php
namespace Core\Site\Response;

use Core\Site\Response\DefaultResponse as BaseResponse;

class ClientResponse extends BaseResponse
{

    function changeLanguage($lang) {
        $this->client->setLang( $lang );
        $this->clearCache();
    }

    public function login()
    {
        $router = \App::getRouter();

        if( $router->getRequestMethod() != 'POST' ) {

            if(\App::getSession()->isLoggedIn()) {
                return $this->reload('/config');
            }

        } else {

            $email = $router->getParameter( 'email', "" );
            $pass = $router->getParameter( 'pass', "" );

            if( !empty($email) && !empty($pass) ) {
                $client = new $this->client_class(array('email'=>$email));

                if ( $client->testPass($pass) ) {
                    \App::getSession()->setUser( $client );
                    return $this->clearCache('/apps');

                } else {
                    $this->addMessage('error.login-invalid');
                }

            } else {
                $this->addMessage('error.login-empty');
            }

        }

        return $this->display('Core:Site:Client:login.html.twig');
    }

    public function resetPassword( $hash = null )
    {
        if( empty($hash) && $this->router->isPOST() ) {
            if( validEmail($email = $this->router->getParameter('email') ) ) {
                $request = new \Core\Site\Model\UserPasswordReset();
                if( $request->createRequest($email) ) {
                    $translator = new \Core\Site\Driver\Translator();
                    $lang = $request->getUser()->getLang();
                    \Core\Site\Provider\Mailer::queueMail(
                            $email,
                            $translator->translate( 'reset-password', array(), 'CoreSite:base' , $lang ),
                            $translator->translate(
                                    'mails.passwordreset',
                                    array(
                                            'link' => $this->router->generate( 'reset-password', array('hash'=>$request->getHash()), true ),
                                            'requestedAt' => date('Y-m-d H:i:s')
                                    ),
                                    'CoreSite:base.xml',
                                    $lang
                            )
                    );
                    \Core\Site\Provider\Mailer::runQueue();
                    $this->reset_mail_sent = $email;

                } else {
                    $this->addMessage('error.email');
                }
            } else {
                $this->addMessage('error.email');
            }

        } elseif( !empty($hash) ) {
            $request = new \Core\Site\Model\UserPasswordReset(array('hash'=>$hash));

            if( $request->getAccountID() > 0 ) {
                $password = $this->router->getParameter('password');
                $confirm = $this->router->getParameter('confirm');

                if( !empty($password) && $password == $confirm ) {
                    $client = new $this->client_class(array('accountID'=>$request->getAccountID()));
                    $client->setPass( md5($password . $client->getSalt()) );
                    $client->storeRow();
                    \App::getSession()->setUser( $client );
                    $request->deleteRow();
                    return $this->clearCache("/");

                } else {
                    $this->ask_for_new_pass = $request->getHash();
                }

            }
        }

        $this->display('Core:Site:Client:resetPassword.html.twig');
    }

    public function facebookLogin() {
        if( $facebookID = $this->app->getFacebook()->getUser() ) {
            $client = new $this->client_class(array('facebookID'=>$facebookID));

            if ( $client->getAccountID() > 0 ) {
                \App::getSession()->setUser( $client );
                $this->clearCache('/');

            } else {
                $this->notFound('facebook.no-user-found');
            }

        } else {
            $this->display('Core:Site:Facebook:login.html.twig');
        }
    }

    public function disallowMails( $email, $hash )
    {
        if( $this->app->getClient()->getEmail() == $email ) {
            $client = $this->app->getClient();
        } else {
            $client = new $this->client_class(array('email'=>$email));
        }

        if( $this->success = $hash == $client->getMailDisallowHash() ) {
            $client->setMailAllowed( false );
            $this->email = $email;
            $this->display('Core:Site:Client:mailsDisallowed.html.twig');
        } else {
            $this->notAllowed();
        }
    }

    function logout() {
        \App::getSession()->logout();
        header("Refresh: 0; url=".curServer().'/');
        setcookie(session_name(),session_id(),time()-60*60*24*7,'/');
    }

    function config()
    {
        $this->display('Core:Site:Client:config.html.twig');
    }

    function configFacebook() {
        $this->status = 'already_connected';

        if ( !$this->client->getFacebookID() ) {
            if( $facebookID = $this->app->getFacebook()->getUser() ) {
                $this->client->setFacebookID( $facebookID );
                $this->status = 'already_connected';
            } else {
                $this->status = 'not_logged_in';
            }

        } elseif( $this->router->getRequestMethod() == 'POST' && $this->router->getParameter('disconnect') !== null ) {
            $this->app->getSession()->resetFacebookParams();
            $this->client->setFacebookID(NULL);
            $this->status = 'not_logged_in';
        }

        $this->display('Core:Site:Facebook:connect.html.twig');
    }

    function changePassword($current = "", $password = "", $confirm = "")
    {
        $router = \App::getRouter();

        if( $router->getRequestMethod() == 'POST' ) {

            if( !empty($password) && $password === $confirm ) {
                if ( $this->client->changePass($current, $password) ) {
                    $this->app->getSession()->addMessage('config.result-pass-changed','CoreSite:base','success', array(), array('data-insertbefore'=>'form[name=pass-change-form]'));
                } else {
                    $this->app->getSession()->addMessage('error.current-mismatch','CoreSite:base','error', array(), array('data-insertafter'=>'#pc_current'));
                }
            } else {
                $this->app->getSession()->addMessage('error.pass-mismatch','CoreSite:base','error', array(), array('data-insertafter'=>'#pc_confirm-pass'));
            }
        }

        $this->redirect( $router->generate('config') );
    }
}
