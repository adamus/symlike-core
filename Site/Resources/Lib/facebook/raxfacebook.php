<?php
require_once "base_facebook.php";

class RAXFacebook extends BaseFacebook {

    function __construct($config) {
        parent::__construct($config);
    }

    protected static $kSupportedKeys =
    array('state', 'code', 'access_token', 'user_id');

    protected function setPersistentData($key, $value) {
        if (!in_array($key, self::$kSupportedKeys)) {
            self::errorLog('Unsupported key passed to setPersistentData.');
            return;
        }

        $session_var_name = $this->constructSessionVariableName($key);
        \App::getSession()->setFacebookParam($session_var_name, $value);
    }

    protected function getPersistentData($key, $default = false) {
        if (!in_array($key, self::$kSupportedKeys)) {
            self::errorLog('Unsupported key passed to getPersistentData.');
            return $default;
        }

        $session_var_name = $this->constructSessionVariableName($key);

        $value = \App::getSession()->getFacebookParam($session_var_name);

        return $value !== false ? $value : $default;
    }

    protected function clearPersistentData($key) {
        if (!in_array($key, self::$kSupportedKeys)) {
            self::errorLog('Unsupported key passed to clearPersistentData.');
            return;
        }

        $session_var_name = $this->constructSessionVariableName($key);
        \App::getSession()->setFacebookParam($session_var_name, null);
    }

    protected function clearAllPersistentData() {
        foreach (self::$kSupportedKeys as $key) {
            $this->clearPersistentData($key);
        }
    }

    protected function constructSessionVariableName($key) {
        return implode('_', array('fb',$this->getAppId(),$key));
    }
}