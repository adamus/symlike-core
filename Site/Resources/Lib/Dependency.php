<?php

# TWIG TEMPLATING
require_once __DIR__ . "/twig/lib/Twig/Autoloader.php";
Twig_Autoloader::register();

# SWIFT MAILER
require_once __DIR__ . "/swiftmailer/swift_required.php";

# FACEBOOK
require_once __DIR__ . "/facebook/raxfacebook.php";