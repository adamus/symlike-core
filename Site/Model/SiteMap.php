<?php
namespace Core\Site\Model;

use Core\Kernel\Driver\CacheController;
use Core\Kernel\Driver\PathLayer;
use Core\Kernel\Model\Cacheable;

/**
 * Sitemap cache.
 *
 * @author Ádám Török (adamus.tork@gmail.com)
 */
class SiteMap extends Cacheable
{
    protected $_maxcache = 300;
	protected $routes = array();
	protected $firewalls = array();

	function __construct() {
	    $this->onCacheLimit();
	}

	function __wakeup() {
	    if( \App::SITE_DEBUG ) {
	        $this->onCacheLimit();
	    }
	    parent::__wakeup();
	}

	function onCacheLimit() {
	    $this->_cached = time();
	    $this->routes = array();
	    $this->firewalls = array();
	    $this->loadRoutes( 'routes.yml' );
	    $this->loadFirewalls('security.yml');
	    CacheController::putSharedObject( $this );
	}

	public function getFirewalls()
	{
	    return $this->firewalls;
	}

	public function getRoutes()
	{
	    return $this->routes;
	}

	public function getRoute( $name )
	{
	    return isset($this->routes[ $name]) ? $this->routes[ $name ] : null;
	}

	public function checkFirewall( $path, &$redirect = null )
	{
	    $path = str_replace(array('{','}'),'',$path);
	    foreach( $this->getFirewalls() as $regex => $protected ) {
	        $match = '@' . $regex . '@i';

	        if( preg_match($match, $path, $matches) && !$this->checkRoles($protected['methods']) ) {
	            if( isset($protected['redirect']))
	                $redirect = $protected['redirect'];
	            return false;
	        }
	    }
	    return true;
	}

	protected function checkRoles( $roles )
	{
	    $user = \App::getClient();
	    foreach( $roles as $role ) {
            if( !$user->hasRole( $role ) ) return false;
	    }
	    return true;
	}

	protected function loadRoutes( $path )
	{
	    $route_file = PathLayer::parseFilePath( $path );
	    if( is_file($route_file) ) {
	        $routes = \Spyc::YAMLLoad($route_file);

	        if( is_array($routes) ) {
	            foreach( $routes as $route_name => $route_spec ) {
	                if( $route_name == 'import' ) {
	                    if( is_array($route_spec) ) {
	                        foreach( $route_spec as $import_path ) {
	                            $this->loadRoutes( $import_path ); // recursive import!
	                        }
	                    } else {
	                        $this->loadRoutes( $route_spec );
	                    }
	                } elseif( isset($route_spec['response']) ) {
	                    if( isset($route_spec['path']) ) {
	                        $route_spec['_match'] = '@^' . $route_spec['path'] . '$@';

	                        # /extract/(:params)/(:from-path)
	                        if( preg_match_all( '(:[\w+-]+)', $route_spec['_match'], $matches ) ) {

	                            # (re-)ordering by path
                                $tmp_params = isset($route_spec['params'])?$route_spec['params']:array();
                                $route_spec['params'] = array();

	                            foreach( $matches[0] as $match ) {
	                                $param = ltrim($match,':');

	                                $_reg = '([^/]+)';
	                                # (re-)add param to ref list
	                                if( isset($tmp_params[$param]) ) {
	                                    if( !isset($tmp_params[$param]['match']) ) $tmp_params[$param]['match'] = $_reg;
	                                    else $_reg = $tmp_params[$param]['match'];
	                                    $tmp_params[$param]['name'] = $param;
	                                    $route_spec['params'][] = $tmp_params[$param];

	                                } else {
	                                    $route_spec['params'][] = array('name'=>$param,'default'=>null,'match'=>$_reg);
	                                }

	                                $route_spec['_match'] = str_replace( $match, $_reg, $route_spec['_match'] );
	                            }
	                        }
	                    }
	                    $this->routes[ $route_name ] = $route_spec;
	                }
	            }

	        } else die("error parsing @ $route_file");

	    } else die("route file not found @ $route_file");
	}

	protected function loadFirewalls( $path )
	{
	    $route_file = PathLayer::parseFilePath( $path );
	    if( is_file($route_file) ) {
	        $firewalls = \Spyc::YAMLLoad($route_file);

	        foreach( (array)@$firewalls['protected'] as $protected ) {
	            $methods = array();
	            /*foreach( explode('|', $protected['role']) as $role ) {
	                $methods[$role]=(string)@$firewalls['roles'][$role];
	            }
	            $this->firewalls[ $protected['path'] ] = array('methods'=>$methods);*/
	            $this->firewalls[ $protected['path'] ] = array('methods'=>explode('|', $protected['role']));
	            if( isset($protected['redirect']) ) {
	                $this->firewalls[ $protected['path'] ]['redirect'] = $protected['redirect'];
	            }
	        }

	        foreach( (array)@$firewalls['presponse'] as $presponse ) {
	            foreach( $this->routes as &$route ) {
	                if( isset($route['path']) && isset($presponse['path']) && preg_match('@'.$presponse['path'].'@', $route['path']) ) {
                        $route['presponses'][] = $presponse;
	                }
	            }
	        }
	    }
	}
}