<?php
namespace Core\Site\Model;

use Core\Kernel\Driver\PathLayer;

/**
 * Generates the menu struct for the current user's roles.
 *
 * @author Ádám Török (adamus.tork@gmail.com)
 */
class UserMenu
{

	protected $menu = array();
	protected $mapped = 0;
	protected $lang = 'hu';

	/**
	 * @param \Core\Site\Model\SiteMap $router
	 */
	function __construct( $router )
	{
	    $this->lang = \App::getClient()->getLang();
	    $this->mapped = time();
	    $nav = PathLayer::loadEscapeJSONFile( PathLayer::parseFilePath('navigation.json') );
		$this->collect_menu( $nav, $router );
	}

	public function getMappedAt()
	{
	    return $this->mapped;
	}

	/**
	 * Collects the _Navigation points_
	 */
	protected function collect_menu( $nav, $router, $link_path = '', $label_path = '' )
	{
	    foreach( $nav as $nav_key => $nav_val ) { // Each navigation point defined in app/navigation.json
	        foreach( $nav_val as $nav_path ) { // Each sub path under the nav point.

	            foreach( $router->getSitemap()->getRoutes() as $route_name => $route_spec ) { // Each route from all imported /routes.yml

	                # Nav points must have name, path parameters and the current user must have access to it.
	                if( !isset($route_spec['path']) || !$router->getSiteMap()->checkFirewall($route_spec['path']) ) continue;

	                if( $nav_path == $route_name ) {
                        $this->menu[ $nav_key ][ $route_name ]= array(
	                        'name' => $route_name,
	                        'label' => \App::getTranslator()->translate( $this->lang . '.' . $route_name, array(), 'navigation' ),
	                        'link' => $route_spec['path'],
                            'params' => isset($route_spec['params'])?$route_spec['params']:array(),
	                        'subpages' => array()
	                    );

                        # Nav points may have childs defined.
                        $this->build_recursive( $router, $this->menu[ $nav_key ][ $route_name ] );
	                }

	            }
	        }
	    }

	}

	/**
	 *  Collects the child route(s) under the given route or nav point.
	 */
	protected function build_recursive( &$router, &$menu_item )
	{
	    foreach( $router->getSitemap()->getRoutes() as $route_name => $route_spec ) { // Each route from all imported /routes.yml

	        # Menu items must have parent, name and path defined and the current user must have access to it.
	       if( !isset($route_spec['parent']) || !isset($route_spec['path']) || !$router->getSiteMap()->checkFirewall($route_spec['path']) ) continue;

	        if( $route_spec['parent'] == $menu_item[ 'name' ] ) {
	            $menu_item[ 'subpages'][ $route_name ]= array(
	                'name' => $route_name,
                    'label' => \App::getTranslator()->translate( $this->lang . '.' . $route_name, array(), 'navigation' ),
	                'link' => $route_spec['path'],
                    'params' => isset($route_spec['params'])?$route_spec['params']:array(),
	                'subpages' => array()
	            );

	            # Routes may have childs defined.
	            $this->build_recursive( $router, $menu_item[ 'subpages' ][ $route_name ] );
	        }
	    }
	}

	/**
	 * Returns the specified _Navigation point_
	 * @param string $menu
	 */
    public function getMenu( $menu )
    {
        return isset( $this->menu[ $menu ] ) ? $this->menu[ $menu ] : null;
    }

}