<?php
namespace Core\Site\Model;

use Core\Kernel\Model\Entity;

/**
 * Az db_accounts egy sora, mint felhasználó.
 *
 * @author Török Ádám (adamus.tork@gmail.com)
 */

class UserBase extends Entity
{
    public static function getEntityTable() { return 'db_accounts'; }
    protected $_maxcache = 60; //cache 1 percig

    function __construct($fetch=false) {
        parent::__construct($fetch);
        $this->loadUserRoles();
    }

    #########################
    # DB
    /**
     * Unique / ID
     * @var integer $accountID
     */
    protected $accountID = NULL;

    /**
     * Unique
     * @var string $email
     */
    protected $email;

    /**
     * @var string $pass
     */
    protected $pass;

    /**
     * @var string $salt
     */
    protected $salt;

    /**
     * @var integer $defaultID
     */
    protected $defaultID = 0;

    /**
     * @var integer $mailTimer
     */
    protected $mailTimer = 0;

    /**
     * @var integer $mailAllowed
     */
    protected $mailAllowed = true;

    /**
     * @var integer $loginFail
     */
    protected $loginFail = 0;

    /**
     * @var integer $flag
     */
    protected $flag = 0;

    /**
     * @var string $lang
     */
    protected $lang = '';

    /**
     * @var integer $facebookID
     */
    protected $facebookID = null;

    public function getAccountID() {
        return (int) $this->accountID;
    }

    public function setAccountID($accountID) {
        $this->accountID = (int) $accountID;
    }

    public function getEmail() {
        return (string) $this->email;
    }

    public function setEmail($email) {
        $this->email = (string) $email;
        return $this;
    }

    public function getPass() {
        return (string) $this->pass;
    }

    public function setPass($pass) {
        $this->pass = (string) $pass;
        $this->_save = true;
        return $this;
    }

    public function testPass($pass) {
        if (!empty($pass) && md5($pass . $this->getSalt()) === $this->getPass()) {
            $this->setLoginFail(0);
            return true;

        } else {
            $fails = $this->getLoginFail();
            $this->setLoginFail(++$fails);

            if ($fails > 30) { //@TODO levenni, ha megvan a bug az exponenciális növekedésre.
                $this->setPass(md5(uniqid() . time() . mt_rand()));
            }

            return false;
        }
    }

    public function changePass($current, $new) {
        if ($this->testPass($current)) {
            $this->setPass(md5($new . $this->getSalt()));
            return true;
        }

        return false;
    }

    public function getSalt() {
        if (empty($this->salt)) {
            $this->salt = md5(time() . uniqid() . mt_rand());
        }
        return (string) $this->salt;
    }

    public function setSalt($to) {
        $this->salt = (string) $to;
        return $this;
    }

    public function getDefaultID() {
        return (int) $this->defaultID;
    }

    public function setDefaultID($characterID) {
        $this->defaultID = (int) $characterID;
        $this->_save = true;
        return $this;
    }

    public function getMailTimer() {
        return (int) $this->mailTimer;
    }

    public function setMailTimer($timer) {
        $this->mailTimer = (int) $timer;
        $this->_save = true;
        return $this;
    }

    public function getMailAllowed() {
        return (int) $this->mailAllowed;
    }

    public function setMailAllowed($allowed) {
        $this->mailAllowed = (boolean)$allowed;
        $this->_save = true;
        return $this;
    }

    public function getMailDisallowHash() {
        return sha1( md5( 'disallow-mails' . $this->getAccountID() . $this->getEmail() . $this->getSalt() ) );
    }

    public function getLoginFail() {
        return (int) $this->loginFail;
    }

    public function setLoginFail($to) {
        $this->loginFail = (int) $to;
        $this->_save = true;
        return $this;
    }

    public function getFlag() {
        return (int) $this->flag;
    }

    public function testFlag($with) {
        $with = (int) $with;
        return ($this->getFlag() & $with) === $with;
    }

    public function setFlag($to) {
        $this->flag = (int) $to;
        $this->_save = true;
        return $this;
    }

    public function addFlag($with) {
        $this->setFlag($this->getFlag() | $with);
        return $this;
    }

    public function removeFlag($with) {
        $this->setFlag($this->getFlag() & ~$with);
        return $this;
    }

    public function getLang() {
        if (empty($this->lang)) {
            switch (getBrowserLang()) {
            case 'en':
                $this->setLang('en');
                break;
            default:
                $this->setLang('hu');
            }
        }
        return (string) $this->lang;
    }

    public function setLang($to) {
        $this->lang = (string) $to;
        $this->_save = true;
        return $this;
    }

    public function getFacebookID() {
        return $this->facebookID;
    }

    public function setFacebookID($facebookID) {
        $this->facebookID = $facebookID;
        $this->_save = true;
        return $this;
    }

    #########################
    # SITE

    protected $_roles = array();

    public function hasRole( $name )
    {
        return in_array($name, $this->_roles);
    }

    protected function loadUserRoles()
    {
        $roles = (array)\App::getDB()->fetch_all('SELECT role FROM ac_user_roles WHERE accountID=?',array($this->accountID));
        foreach( $roles as $role ) {
            if( !in_array($role['role'],$this->_roles) ) $this->_roles[] = $role['role'];
        }

        if( $this->accountID && !in_array('USER',$this->_roles) ) $this->_roles[] = 'USER';
    }

    public function createClient($email, $pass)
    {
        if ($already_exists = $this->queryRow(array('email' => $email))) {
            $this->loadRow($already_exists);
            $this->setPass(md5($pass . $this->getSalt()));
            return true;
        } else {
            $this->setEmail($email);
            if (validEmail($email)) {
                $this->setPass(md5($pass . $this->getSalt()));
                $this->setAccountID($this->storeRow());
                return true;
            }
        }
        return false;
    }

    public function clearCache()
    {
        \App::getSession()->clearCache();
    }

}
