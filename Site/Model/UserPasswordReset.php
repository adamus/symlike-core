<?php
namespace Core\Site\Model;

use Core\Kernel\Model\Entity;

class UserPasswordReset extends Entity {
    public static function getEntityTable() {
        return 'ac_pwreset_hash';
    }

    protected $accountID;
    protected $hash;
    protected $requestedAt;

    public function getAccountID() {
        return $this->accountID;
    }

    public function setAccountID($accountID) {
        $this->accountID = $accountID;
    }

    public function getHash() {
        return $this->hash;
    }

    public function setHash($hash) {
        $this->hash = $hash;
    }

    public function getRequestedAt() {
        return $this->requestedAt;
    }

    public function setRequestedAt($requestedAt) {
        $this->requestedAt = $requestedAt;
    }

    public function getUser()
    {
        $user = new \Core\Site\Model\UserBase(array('accountID'=>$this->getAccountID()));
        return $user;
    }

    public function createRequest( $email )
    {
        $user = new \Core\Site\Model\UserBase(array('email'=>$email));
        if( $user->getAccountID() > 0 ) {
            $this->setAccountID( $user->getAccountID() );
            $this->setHash( md5(time() . uniqid() . mt_rand()) );
            $this->setRequestedAt( time() );
            $this->storeRow();
            return true;
        }
        return false;
    }

}
