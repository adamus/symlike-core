<?php
namespace Core\Site;

use Core\Kernel\App as BaseApp;
use Core\Kernel\Driver\CacheController;

use Core\Site\Provider\Session;
use Core\Site\Driver\Router;
use Core\Site\Provider\TwigLoader;


/**
 * Site-level global functions.
 *
 * @author Ádám Török (adamus.tork@gmail.com)
 */
class App extends BaseApp
{

    /**
     * @var \Core\Site\Driver\Router
     */
    protected static $router;

    /**
     * @var \Core\Site\Provider\Session $session
     */
    protected static $session;

    /**
     * @var \Twig_Environment $twig
     */
    protected static $twig;

    /**
     * @var \RAXFacebook
     */
    protected static $facebook;

    /**
     * \Swift_SmptTransport $mailer
     */
    protected static $mailer;

    /**
     * @var \Core\Site\Driver\Translator $translator
     */
    protected static $translator;

    /**
     * @return \Core\Site\Driver\Router
     */
    public static function getRouter()
    {
        if (!is_object(self::$router)) {
            self::$router = new Router();
        }
        return self::$router;
    }

    public static function executeRequest()
    {
        $request = self::getRouter()->request();
        $session = self::getSession();
        self::getRouter()->executeRequest( rtrim($request, '/') );
    }

    /**
     * @return \Twig_Environment
     */
    public static function getTwig()
    {
        if (!is_object(self::$twig)) {
            if( self::TWIG_CACHE ) {
                self::$twig = new \Twig_Environment(new TwigLoader(self::getDir('twig_files')) ,array('cache'=>self::getDir('twig_cache')));
            } else {
                self::$twig = new \Twig_Environment(new TwigLoader(self::getDir('twig_files')));
            }
            self::$twig->addExtension( new \Core\Site\Provider\TwigExtension() );
        }

        return self::$twig;
    }

    public static function getFacebook()
    {
        if (!is_object(self::$facebook)) {
            self::$facebook = new \RAXFacebook(array(
                'appId'  => self::$fb_appid,
                'secret' => self::$fb_secret,
            ));
        }
        return self::$facebook;
    }

    /**
     * @return \Core\Site\Provider\Session
     */
    public static function getSession()
    {
        if (!is_object(self::$session)) {
            self::$session = new Session();
        }
        return self::$session;
    }

    /**
     * @return \Core\Site\Model\UserBase;
     */
    public static function getClient()
    {
        return self::getSession()->getUser();
    }

    /**
     * @return \Swift_Mailer
     */
    public static function getMailer()
    {
        if(!is_object(self::$mailer)) {
            self::$mailer = \Swift_Mailer::newInstance(
                \Swift_SmtpTransport::newInstance(self::$mailer_host, self::$mailer_port, self::$mailer_encryption)
                    ->setUsername(self::$mailer_user)
                    ->setPassword(self::$mailer_password)
            );
        }
        return self::$mailer;
    }

    /**
     * @return \Core\Site\Driver\Translator
     */
    public static function getTranslator()
    {
        if (!is_object(self::$translator)) {
            self::$translator = CacheController::getSharedObject('\\Core\\Site\\Driver\\Translator');
        }
        return self::$translator;
    }

    public static function getClientClass()
    {
        return '\\Core\\Site\\Model\\UserBase';
    }

    public static function clearCache( $drop = false )
    {
        if( $drop ) CacheController::dropCache();
        self::getClient()->clearCache();
    }

}