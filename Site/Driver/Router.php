<?php
namespace Core\Site\Driver;

use Core\Kernel\Driver\CacheController;
use Core\Kernel\Driver\PathLayer;
use Core\Site\Model\SiteMap;
use Core\Site\Model\UserMenu;

class Router
{

	/**
	 * @var SiteMap
	 */
	protected $sitemap;
	protected $preindex = '';
	protected $current_route = null;
	protected $current_request = null;

	public function getCurrentRoute()
	{
	    return $this->current_route;
	}

	public function getCurrentRequest()
	{
	    return $this->current_request;
	}

	/**
	 * Vissza adja az útválasztót.
	 * @return SiteMap
	 */
	function getSitemap()
	{
		if( empty($this->sitemap) ) {
			$this->sitemap = CacheController::getSharedObject( '\\Core\\Site\\Model\\SiteMap' );
		}
		return $this->sitemap;
	}

	public function generate( $path_name, $arguments = array(), $include_server = false )
	{
	    $prefix = $include_server ? $this->getCurrentHost() . $this->preindex : $this->preindex;
		$route = $this->getRouteByName( $path_name );
		if( isset($route['path']) ) {
		    $path = $route['path'];
		    foreach( $arguments as $k => $v ) {
		        $path = str_replace( ':'.$k, $v, $path );
		    }
		    if( isset($route['params']) ) {
		        foreach( (array)$route['params'] as $dv ) {
		            if( isset($dv['default']) && $dv['default'] !== false ) {
		                $path = str_replace( ':'.$dv['name'], $dv['default'], $path );
		            }
		        }
		    }
		    return $prefix . $path;
		}
		return $prefix;
	}

	/**
	 * Vissza adja az aktuális felhasználó számára elérhető oldaltérképet... menüt.
	 * @return UserMenu
	 */
	public function getUsermenu()
	{
		if ( \App::SITE_DEBUG || !is_object(\App::getSession()->usermenu) || \App::getSession()->usermenu->getMappedAt() < time() - 120) {
			\App::getSession()->usermenu = new UserMenu( $this );
		}
		return \App::getSession()->usermenu;
	}

	public function getRouteByName( $name )
	{
	    return $this->getSitemap()->getRoute( $name );
	}

	public function executeRequest( $request_path )
	{
	    $request_path = urldecode($request_path);
	    $this->current_request = $request_path;
	    if( !$this->getSiteMap()->checkFirewall( $request_path, $redirect ) ) {
	        return $this->executeRoute( $this->getRouteByName( $this->current_route = empty($redirect) ? 'not-allowed' : $redirect ) );
	    }

	    foreach( $this->getSitemap()->getRoutes() as $route ) {
	        if( isset($route['_match']) && isset($route['response']) && preg_match( $route['_match'], $request_path, $matches) ) {
	            $this->current_route = $route;
	            return $this->executeRoute( $route, $matches );
	        }
	    }

	    $this->current_route = 'not-found';
	    return $this->executeRoute( $this->getRouteByName('not-found') );
	}

	/**
	 * @param array $route
	 * @param array $matches array list of parameters preg_matched from the url
	 * @return mixed|booleean depends on response method's return
	 */
	public function executeRoute( $route, $matches = array() )
	{
	    $response = PathLayer::parseResponsePath( $route['response'] ); // Core.Site.Default.index => [\Core\Site\Response\DefaultResponse,index]
	    $response[2]= count($matches)?array_slice($matches, 1):array(); // /this/(:has)/(:params) -> [:has,:params]

	    if( $this->isPOST() ) {
	        if( isset($route['validator']['token']) && $route['validator']['token'] == true && !\App::getSession()->testToken() )
	            return $this->sendTo('not-allowed');

	        if( isset($route['validator']['has_post']) ) {
	            foreach( $route['validator']['has_post'] as $post_key ) {
	                $post_value = $this->getParameter( $post_key );
	                if( $post_value === null )
	                    return $this->sendTo('not-allowed');
	                $response[2][]=$post_value;
	            }
	        }
	    }

	    $response_ob =  new $response[0]();
	    $response_call = array($response_ob, $response[1] ); // [\Core\Site\Response\DefaultResponse,index] -> \Core\Site\Response\DefaultResponse()->index()

	    if( is_callable($response_call) ) { // checks if "\Core\Site\Response\DefaultResponse()->index()" is real
	        if( isset($route['presponses']) && is_array($route['presponses']) ) {
	            foreach( $route['presponses'] as $_pre ) {
	                if( isset($_pre['method']) ) {
	                    $presponse_call = array($response_ob,$_pre['method']);
	                    if( is_callable($presponse_call) && !call_user_func_array($presponse_call, $response[2]) )
	                        return $this->sendTo('not-allowed');
	                }
	            }
	        }

	        return call_user_func_array( $response_call, $response[2] );
	    }

	    return $this->sendTo('not-found');
	}

	public function sendTo( $route )
	{
	    $this->current_route = $route;
	    return $this->executeRoute( $this->getRouteByName($route) );
	}

	public function getRequestMethod()
	{
	    return isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;
	}

	public function isPOST()
	{
	    return $this->getRequestMethod() === 'POST';
	}

	public function getCurrentHost()
	{
	    $serverURL = 'http';
	    if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") $serverURL .= "s";
	    return $serverURL .= "://".\App::HOST;
	}

	public function getUrlParameter( $name, $default = null )
	{

	}

	public function getParameter( $name, $default = null, $strip = false, $filter = null )
	{
	    if( isset($_POST[ $name ]) ) {
	        $param = $strip ? strip_tags($_POST[ $name ]) : $_POST[ $name ];
	        if( is_array($filter) && !in_array($param, $filter) ) {
                return $default;
	        } else {
	            return $param;
	        }
	    } else {
	        return $default;
	    }
	}

	public function request()
	{
	    if( !isset($_SERVER["REQUEST_URI"]) || !isset($_SERVER['SCRIPT_NAME']) ) return null;
		if (!\App::REWRITE || strstr($_SERVER["REQUEST_URI"], $_SERVER['SCRIPT_NAME']) !== false) {
			$this->preindex = isset($_SERVER['SCRIPT_NAME'])?$_SERVER['SCRIPT_NAME']:'';
			return str_replace($this->preindex,'',isset($_SERVER["REQUEST_URI"])?$_SERVER["REQUEST_URI"]:'');

		} elseif(isset($_SERVER['SCRIPT_NAME'])&&isset($_SERVER["REQUEST_URI"])) {
			$cut_subdir = strrpos($_SERVER['SCRIPT_NAME'],'/');
			$request = substr($_SERVER["REQUEST_URI"], $cut_subdir, strlen($_SERVER["REQUEST_URI"])-$cut_subdir);
			$this->preindex = substr($_SERVER['SCRIPT_NAME'], 0, $cut_subdir);
			return $request;

		} else {
		    return null;
		}
	}

	public function preindex()
	{
		return $this->preindex;
	}

	public function assets()
	{
		return str_replace('/index.php', '', $this->preindex) . '/src/';
	}

	public function getRoute($pathway, $full=false)
	{
		return ($full?curServer():'') . $this->preindex . '/' . ltrim($pathway,'/');
	}
}
