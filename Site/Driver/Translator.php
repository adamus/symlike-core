<?php
namespace Core\Site\Driver;

use Core\Kernel\Driver\PathLayer;
use Core\Kernel\Entity\SharedCache;

class Translator extends SharedCache
{
    protected $_maxcache = 43200;

    function __construct()
    {
        $this->onCacheLimit();
    }

    function onCacheLimit()
    {
        $this->_cached = time();
        $this->_cached_data = array();
    }

    public function translate( $node, $args = array(), $module = 'CoreSite:base', $lang = null )
    {
        $lang = $lang === null ? \App::getClient()->getLang() : $lang;

        if( $module == 'navigation' ) {

            if( !isset($this->_cached_data['navigation']) ) {
                $this->_cached_data['navigation'] = $translation = parse_ini_file( PathLayer::parseFilePath('navigation.ini'), true );

            } else {
                $translation = $this->_cached_data['navigation'];

            }

        } else {

            if( !isset($this->_cached_data[$lang][$module]) ) {
                if( !isset($this->_cached_data[$lang] ) ) $this->_cached_data[$lang] = array();

                if( substr($module, -3 ) == 'xml' ) {
                    $this->_cached_data[$lang][$module] = $translation = $this->xml2array( simplexml_load_file( PathLayer::parseTranslationPath( $module, $lang ) ));

                } elseif( substr($module, -3 ) == 'yml' ) {
                    $this->_cached_data[$lang][$module] = $translation = \Spyc::YAMLLoad( PathLayer::parseTranslationPath( $module, $lang ) );

                } else {
                    $this->_cached_data[$lang][$module] = $translation = json_decode( file_get_contents(  PathLayer::parseTranslationPath( $module, $lang ) ), true );

                }

            } else {
                $translation = $this->_cached_data[$lang][$module];

            }

        }

        $nodes = explode( '.', $node );
        if( $translation ) {
            foreach( $nodes as $selector ) {
                if( is_object($translation) && property_exists( $translation, $selector) ) {
                    $translation = $translation->$selector;

                } elseif( is_array($translation) && isset($translation[$selector]) ) {
                    $translation = $translation[$selector];

                } else
                    return $node;
            }
            return $this->replace( (string)$translation, $args );
        }
        return $node;
    }

    /**
     * @param SimpleXmlElement $xml
     */
    protected function xml2array( $xml )
    {
        if( $xml->count() ) {
            $array = array();
            foreach( $xml->children() as $node ) {
                $array[ $node->getName() ] = $this->xml2array($node);
            }
            return $array;
        } else {
            return $xml->__toString();
        }
    }

    public function replace( $text, $args = array() )
    {
        foreach( $args as $k => $v ) {
            $text = str_replace( '%' . $k . '%', $v, $text );
        }
        return $text;
    }
}