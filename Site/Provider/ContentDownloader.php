<?php
namespace Core\Site\Provider;

class ContentDownloader
{
    /**
     * Download data from the given URL
     *
     * @param string $url path:://to.the.remote/resource
     * @param integer $timeout allowed seconds without response
     * @param integer $retry number of retries
     * @param integer $sleep 1/100 of seconds of sleep between retries
     *
     * @throws \Exception
     *
     */
    public static function download( $url, $timeout = 5, $retry = 0, $sleep = 100 )
    {
        if( !\App::checkExecutionTime(0.8) ) throw new \Exception( 'timeout', 1 );

        $request = parse_url($url);
        if( !isset($request['host']) || !isset($request['path']) ) return null;

        if( isset($request['scheme']) && $request['scheme'] == 'https' ) {
            $socket_host = "ssl://" . $request['host'];
            $socket_port = isset($request['port']) ? $request['port'] : 443;
        } else {
            $socket_host = $request['host'];
            $socket_port = isset($request['port']) ? $request['port'] : 80;
        }

        $path = $request['path'];
        if( isset($request['query']) && !empty($request['query']) ) {
            $path .= '?'.$request['query'];
        }

        $cmd = "GET {$path} HTTP/1.1\r\n";
        $cmd .= "Host: {$request['host']}\r\n";
        $cmd .= "Connection: Close\r\n\r\n";


        $file = '';
        $timer = 0;
        $exception = null;

        do {
            if( $exception instanceof  \Exception ) usleep($sleep * 1000);

            try {
                $sock = @fsockopen( $socket_host, $socket_port, $errno, $errstr, $timeout );
                if( !$sock ) {
                    $exception = new \Exception( "fsockopen", $errno );
                    continue;
                }

                $exception = null;
                $file = '';
                $cycle = microtime(true);

                fwrite($sock, $cmd);
                stream_set_blocking($sock, 0); // fgets must be non-blocking so we can break it on (manual) timeout ~ our server had difficulty reading the stream's metadata

                while( !feof($sock) ) {
                    $rec = fgets($sock, 1024);
                    if( !empty($rec) ) {
                        $file .= $rec;
                        $cycle = microtime(true); // restart timer on response (large transfers could timeout)
                    } elseif( $timeout < microtime(true) - $cycle ) {
                        $exception = new \Exception( 'timeout', 2 );
                        break;
                    }
                }
                fclose($sock);

            } catch( \Exception $e ) {
                $exception = $e;
                //throw new \Exception( $e->getMessage() . " [{$url}]", $e->getCode(), $e->getPrevious() );
            }

        } while( $retry-- > 0 && ( $exception instanceof \Exception || empty($file) ) ); // if retry is allowed and something is wrong

        if( $exception instanceof \Exception ) throw $exception;
        return self::getContentBody($file);
    }

    protected static function getContentBody( $str )
    {
        if( empty($str) ) throw new \Exception( 'empty-response' );

        $parts = explode("\r\n\r\n", $str);
        if( !is_array($parts) || count($parts) < 2 ) throw new \Exception( 'invalid-response' );

        $header = $parts[0];
        if( !preg_match('#HTTP/\d+\.\d+ (\d+)#', $header, $matches) ) throw new \Exception( 'no-status-code' );
        if( $matches[1] > 200 ) throw new \Exception( 'status: ' . $matches[1] );

        $body = $parts[1];
        if( preg_match('#Transfer-Encoding: (\w+)#', $header, $matches) ) {
            if( $matches[1] == 'chunked' ) {
                $body = self::unchunkHttpResponse($body);
            }
        }

        return $body;
    }

    protected static function unchunkHttpResponse($str=null) {
        if (!is_string($str) or strlen($str) < 1) {
            return false;
        }
        $eol = "\r\n";
        $add = strlen($eol);
        $tmp = $str;
        $str = '';
        do {
            $tmp = ltrim($tmp);
            $pos = strpos($tmp, $eol);
            if ($pos === false) {
                return false;
            }
            $len = hexdec(substr($tmp,0,$pos));
            if (!is_numeric($len) or $len < 0) {
                return false;
            }
            $str .= substr($tmp, ($pos + $add), $len);
            $tmp  = substr($tmp, ($len + $pos + $add));
            $check = trim($tmp);
        } while(!empty($check));
        unset($tmp);
        return $str;
    }

}