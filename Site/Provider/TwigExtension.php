<?php
namespace Core\Site\Provider;

use Core\Kernel\Driver\PathLayer;
use Core\Site\Driver\Translator;

class TwigExtension extends \Twig_Extension
{

    /**
     * @var \Core\Site\Driver\Translator $translator
     */
    protected $translator = null;

    function __construct()
    {
        $this->translator = \App::getTranslator();
    }

    /**
     * Returns a list of filters.
     *
     * @return array
     */
    public function getFilters()
    {
        $filters = array(
                'preindex' => new \Twig_Filter_Method($this, 'preindexFilter'),
                'args' => new \Twig_Filter_Method($this, 'argsFilter'),
                'ucfirst' => new \Twig_Filter_Method($this, 'ucfirstFilter'),
                'implode' => new \Twig_Filter_Method($this, 'implodeFilter'),
                'explode' => new \Twig_Filter_Method($this, 'explodeFilter')
        );

        return $filters;
    }

    public function getFunctions()
    {
        $functions = array(
                'asset' => new \Twig_Function_Method($this, 'assetMethod'),
                'path' => new \Twig_Function_Method($this, 'pathMethod'),
                'menuBuilder' => new \Twig_Function_Method($this, 'menuBuilderMethod'),
                'trans' => new \Twig_Function_Method($this, 'transMethod')
        );

        return $functions;
    }

    public function ucfirstFilter( $text )
    {
        return ucfirst($text);
    }

    public function implodeFilter( $array, $glue = ', ' )
    {
        return implode($glue, $array);
    }

    public function explodeFilter( $string, $del = ',' )
    {
        return explode( $del, $string );
    }

    public function argsFilter( $text, $args = array() )
    {
        foreach( $args as $k => $v ) {
            $text = str_replace( '%' . $k . '%', $v, $text );
        }
        return $text;
    }

    public function transMethod( $node, $args = array(), $module = 'CoreSite:base' )
    {
        return $this->translator->translate( $node, $args, $module );
    }

    public function preindexFilter( $path )
    {
        return \App::getRouter()->preindex() . $path;
    }

    public function assetMethod( $path )
    {
        return \App::getRouter()->assets()  . $path;
    }

    public function pathMethod( $path, $arguments = array() )
    {
        return \App::getRouter()->generate( $path, $arguments );
    }

    public function menuBuilderMethod( $type, $method, $arguments = array() )
    {
        $layer = new \Core\Kernel\Driver\PathLayer();
        $class = $layer->parseClassPath( $type  ) . '\\Provider\\MenuBuilder';
        try {
            $builder = new $class();

            $call = array($builder, $method);
            return is_callable( $call ) ? call_user_func_array( $call, $arguments ) : null;

        } catch( \Exception $e ) {
            return $e->getMessage();
        }
    }

    public function getName()
    {
        return 'site_twig_extensions';
    }

}