<?php
namespace Core\Site\Provider;

use Core\Site\Model\UserBase;

class Session {

    protected $session;

    function __construct() {
        $this->session =& $_SESSION;

        if ( !isset($this->session['start']) ) $this->session['start'] = time();
        if ( !isset($this->session['_token']) ) $this->session['_token'] = md5(uniqid().time().mt_rand());
        if ( !isset($this->session['data']) ) $this->session['data'] = array();
        if ( !isset($this->session['fb']) ) $this->session['fb'] = array();
        $this->session['last_ip'] = @$_SERVER['REMOTE_ADDR'];
    }

    public function logout() {
        $this->session = array();
    }

    /**
     * @return boolean
     */
    public function testToken() {
        return isset($_POST['_token']) && $_POST['_token'] == $this->session['_token'];
    }

    public function isLoggedIn() {
        return $this->getUser()->getAccountID() > 0;
    }

    public function getToken() {
        return $this->session['_token'];
    }

    public function clearCache()
    {
        $this->session['data']= array( 'lastroute' => @$this->session['data']['lastroute'] );
    }

    /**
     * @return \Core\Site\Model\UserBase;
     */
    public function getUser() {
        return isset($this->session['user']) ? $this->session['user'] : $this->session['user'] = new UserBase();
    }

    /**
     * @param \Core\Site\Model\UserBase $user
     */
    public function setUser( UserBase $user ) {
        $this->session['user'] = $user;
    }

    public function setFacebookParam( $name, $value )
    {
        if(isset($this->session['fb'])) $this->session['fb'][$name] = $value;
    }

    public function getFacebookParam( $name )
    {
        return isset($this->session['fb'][$name]) ? $this->session['fb'][$name] : null;
    }

    public function resetFacebookParams()
    {
        $this->session['fb']=array();
    }

    public function addMessage( $message, $translation = 'CoreSite:base', $class = 'error', $paramteres = array(), $attributes = array() )
    {
        if( !isset($this->session['_messages']) )
            $this->session['_messages'] = array();

        $this->session['_messages'][]=array(
            'class'           => $class,
            'message'         => $message,
            'translation'     => $translation,
            'parameters'      => $paramteres,
            'attributes'      => $attributes
        );
    }

    public function flushMessages()
    {
        $messages = array();
        if( isset($this->session['_messages']) ) {
            $messages = $this->session['_messages'];
            unset($this->session['_messages']);
            return $messages;
        }
        return $messages;
    }

    function set($name, $value) {
        if( !is_array($this->session) ) {
            $this->session = array('data'=>array());
        }
        if( is_array($this->session) ) {
            $this->session['data'][$name] = $value;
        }
    }

    function &get($name) {
        $stuff = isset($this->session['data'][$name])?$this->session['data'][$name]:false;
        return $stuff;
    }

    function __set($name, $value) {
        if( is_array($this->session) ) {
            $this->session['data'][$name] = $value;
        }
    }

    function &__get($name) {
        $stuff = isset($this->session['data'][$name])?$this->session['data'][$name]:false;
        return $stuff;
    }

}