<?php
namespace Core\Site\Provider;

class MenuBuilder
{

    /**
     * Generates menu structure with the specifiend _Navigation point_
     *
     * @param string $name
     * @param array $parameters
     */
    public function generateMenu( $name, $parameters = null )
    {
        $menu = \App::getRouter()->getUsermenu()->getMenu( $name );

        if ( is_array($menu)  ) {
            $this->paramRecursiveMenu( $menu, $parameters );
            return $menu;
        }

        return array();
    }

    /**
     * Pass the parameters to the menu items.
     */
    protected function paramRecursiveMenu( &$menu, $parameters )
    {
        foreach( $menu as &$route ) {
            $defaults = isset($route['params']) ? $route['params'] : array();

            if( isset($route['label']) ) $this->fillParameters( $route['label'], $parameters, $defaults );
            if( isset($route['link']) ) $this->fillParameters( $route['link'], $parameters, $defaults );

            if ( !empty($route['subpages']) ) {
                $this->paramRecursiveMenu( $route['subpages'], $parameters );
            }
        }

    }

    /**
     * Fill the route's parameters with specified params or defaults.
     */
    protected function fillParameters( &$item, $parameters, $defaults )
    {
        if ( preg_match_all( '(:[\w+-]+)', $item, $matches) ) {
            foreach( $matches[0] as $match ) {
                $param = ltrim($match,':');

                if( is_array($parameters) && isset( $parameters[ $param ] )) {
                    $item = str_replace(':'.$param, $parameters[ $param ], $item);

                } elseif( isset( $defaults[ $param ]['default'] ) && $defaults[ $param ]['default'] !== false ) {
                    $item = str_replace(':'.$param, $defaults[ $param ]['default'], $item);

                } else {
                    $item = substr($item, 0, strpos($item, ':'.$param));

                }
            }
        }
    }

}