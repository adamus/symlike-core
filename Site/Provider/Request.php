<?php
namespace Core\Site\Provider;

class Request {

    protected $preindex;

    public function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

	public function getUri() {
		if (!\App::REWRITE || strstr($_SERVER["REQUEST_URI"], $_SERVER['SCRIPT_NAME']) !== false) {
			$this->preindex = $_SERVER['SCRIPT_NAME'];
			return str_replace($this->preindex,'',$_SERVER["REQUEST_URI"]);
		} else {
			$cut_subdir = strrpos($_SERVER['SCRIPT_NAME'],'/');
			$request = substr($_SERVER["REQUEST_URI"], $cut_subdir, strlen($_SERVER["REQUEST_URI"])-$cut_subdir);
			$this->preindex = substr($_SERVER['SCRIPT_NAME'], 0, $cut_subdir);

			return $request;
		}
	}

}