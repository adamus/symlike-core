<?php
namespace Core\Site\Provider;

class Mailer
{
    public static function queueMail( $address, $subject, $body ) {
        if(!validEmail($address)) return false;
        $subject = \App::$mailer_subject_prefix . $subject;

        \App::getDB()->insert(
                'db_mails',
                array(
                        'address'    => $address,
                        'subject'     => $subject,
                        'body'         =>
                            "<html>
                                <head>
                                    <meta http-equip=\"Content-Type\" content=\"text/html; charset=utf-8\" />
                                    <title>{$subject}</title>
                                </head>
                                <body>{$body}</body>
                            </html>",
                        'altbody'	    => strip_tags( $body )
                )
        );

        return true;
    }

    public static function runQueue() {
        if(!is_array($queue=\App::getDB()->fetch_all('SELECT * FROM db_mails WHERE 1 LIMIT '.\App::getSetup('mail_percron', 50)))) return false;

        foreach($queue as $mailrow) {
            if(empty($mailrow['address'])||isset($mailrow['address'])&&self::send($mailrow)) {
                \App::getDB()->exec("DELETE FROM db_mails WHERE mailID=${mailrow['mailID']}");
            }
        }
    }

    public static function queueFinish() {
        $date = getdate(time());

        $len = floor(@current(\App::getDB()->fetch_first('SELECT COUNT(mailID) FROM db_mails WHERE 1 LIMIT 1'))/\App::getSetup('mail_percron', 50))+1;
        $next = mktime($date['hours'],floor($date['minutes']/10)*10+$len*10,0,$date['mon'],$date['mday'],$date['year']);

        return date('Y-m-d H:i',$next);
    }

	protected static function send($mailrow) {
	    try {
    	    $message = \Swift_Message::newInstance($mailrow['subject'])
    	        ->setFrom(array( \App::$mailer_sender_email => \App::$mailer_sender_name ))
    	        ->setTo(array($mailrow['address']))
    	        ->setBody($mailrow['body'], 'text/html')
    	        ->addPart($mailrow['altbody'], 'text/plain')
    	    ;
    	    \App::getMailer()->send($message, $fail);
    	    return empty($fail);
	    } catch(\Exception $e) {
	        return false;
	    }
	}

	public static function send_backup($file) {
        // @TODO backup send
	}
}
