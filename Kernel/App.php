<?php
namespace Core\Kernel;

use Core\Kernel\Driver\DatabaseConnection;
use Core\Kernel\Driver\DatabaseCache;
use Core\Kernel\Entity\DatabaseSchema;

require_once 'CommonFunctions.php';

/**
 * @var \Core\Kernel\Driver\DatabaseConnection $mysql
 */
$mysql = null;
/**
 * @var \Core\Kernel\Driver\DatabaseCache $db_cache
 */
$db_cache = null;
/**
 * @var \Core\Kernel\Entity\DatabaseSchema $db_schema
 */
$db_schema = null;
/**
 * @var \Core\Kernel\Model\Collector $setup
 */
$setup = null;


/**
 * Kernel-level global functions.
 *
 * @author Ádám Török (adamus.tork@gmail.com)
 */
class App extends \Config {

    /**
     * @param $cache int Cache in minutes
     * @return \Core\Kernel\Driver\DatabaseConnection
     */
    public static function getDB($cache = 0) {
        global $mysql;
        global $db_cache;
        if(!$mysql) $mysql = new DatabaseConnection(self::$mySQL_adr, self::$mySQL_user, self::$mySQL_pass, self::$mySQL_db);
        if ($cache > 0) {
            if(!$db_cache) $db_cache = new DatabaseCache($mysql, 1);
            $db_cache->setLimit( $cache );
            return $db_cache;
        } else {
            return $mysql;
        }
    }

    /**
     * @return \Core\Kernel\Entity\DatabaseSchema
     */
    public static function getSchema()
    {
        global $db_schema;

        if(!$db_schema)
            $db_schema = \Core\Kernel\Driver\CacheController::getSharedObject( '\\Core\\Kernel\\Entity\\DatabaseSchema' );
        return $db_schema;
    }

    /**
     * @return \Core\Kernel\Model\Collector
     */
    public static function getSetup($name = '', $default = null) {
        global $setup;
        if(!$setup) $setup = \Core\Kernel\Driver\CacheController::getSharedObject( '\\Core\\Kernel\\Entity\\CachedSetup' );

        if (!empty($name)) {
            $state = $setup->findByName( $name );
            return is_object($state) ? $state->value : $default;
        } else {
            return $setup;
        }
    }

    public static function log( $line, $type = 'debug' )
    {
        $log_dir = self::getDir('logs_root');
        !@is_dir($log_dir) && @mkdir($log_dir, 0777, true);
        @file_put_contents( $log_dir . DS . $type . '.txt', date('Y-m-d H:i:s') . ': ' . $line . "\n", FILE_APPEND );
    }


    public static function getDir( $alias ) {
        $DS = DS;
        $root = self::getDocumentRoot();
        switch( $alias ) {
            case 'cache_root': return $root . self::CACHE; break;
            case 'logs_root': return "{$root}app/logs"; break;
            case 'database_cache': return $root . self::CACHE . "/queries"; break;
            case 'object_cache': return $root. self::CACHE . "/objects"; break;
            case 'twig_cache': return $root. self::CACHE . "/templates"; break;
            case 'twig_files': return array("{$root}app","{$root}modules"); break;
        }
    }

    public static function getDocumentRoot()
    {
        $root = self::ROOT;
        return empty($root) ? $_SERVER['DOCUMENT_ROOT'] : self::ROOT;
    }

    public static function toDir( $path ) {
        $path = str_replace('\\', DS, $path);
        $path = str_replace('/', DS, $path);
        return $path;
    }

    public static function debug($msg,$fn) {
        if(self::SITE_DEBUG) {
            self::log( "{$fn}() {$msg}" );
        }
    }

    public static function time() { return time(); }

    protected static $_start = 0;

    public static function runTime()
    {
        if( !self::$_start ) {
            self::$_start = microtime(true);
            return 0;
        } else {
            return microtime(true) - self::$_start;
        }
    }

    public static function checkExecutionTime($p=1)
    {
        $m = $p <= 1 ? $p : 1;
        return self::MAX_EXECUTION_TIME * $m > self::runTime();
    }

}
