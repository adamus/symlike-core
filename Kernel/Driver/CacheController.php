<?php
namespace Core\Kernel\Driver;

class CacheController
{
    protected static function sharedObjectNameConvert( $object_type, $id = null )
    {
        return strtolower(preg_replace('/[^\w+]/', '-', ltrim( $object_type, '\\' )) .($id!==null?'_'. md5(json_encode($id)):''));
    }

    public static function getSharedObject( $object_type, $id = null )
    {
        $cache_dir = \App::getDir('object_cache');
        if( !is_dir($cache_dir) && !mkdir($cache_dir, 0777, true) ) {
            die($cache_dir);
            return new $object_type( $id );
        }

        if( $cached = self::retreive( $cache_dir . DS . self::sharedObjectNameConvert($object_type,$id)) ) {
            return $cached;
        } else {
            return new $object_type( $id );
        }
    }

    public static function putSharedObject( $object, $id = null )
    {
        $cache_dir = \App::getDir('object_cache');
        if( !is_dir($cache_dir) ) mkdir($cache_dir, 0777, true);
        $save_path = $cache_dir . DS . self::sharedObjectNameConvert(get_class($object),$id);
        return self::store( $save_path, $object );
    }

	public static function retreive($file, $unserialize = true) {
		$file = str_replace('..','',$file).'.php';

		if (!@file_exists($file)) {
			return false;
		}

		if (!($handle = fopen($file, 'rb'))) {
			return false;
		}

		fgets($handle);

		$data = false;
		$line = 0;

		$buffer = fgets($handle);
		$buffer = substr($buffer, 0, -1);

		$bytes = (int) $buffer;

		$data = fread($handle, $bytes);

		fread($handle, 1);

		/*if (!feof($handle)) {
			$data = false;
		}*/

		fclose($handle);

		if ($data === false) {
			return false;
		} else if($unserialize) {
			$data = unserialize($data);
		}

		return $data;
	}

	public static function store($file, $data = null, $serialize = true) {
		$file = str_replace('..','',$file).'.php';

        if($handle = @fopen($file, 'wb')) {
			flock($handle, LOCK_EX);

			fwrite($handle, '<' . '?php exit; ?' . ">\n");

			if ($serialize) {
				$data = serialize($data);
			}

			fwrite($handle, strlen($data) . "\n");
			fwrite($handle, $data);

			flock($handle, LOCK_UN);
			fclose($handle);

			return true;
		}

		return false;
	}

	# http://www.php.net/manual/en/function.unlink.php#78134
	public static function clearCache() {
		$seconds_old = 60*60*24;
		$directory = \App::getDir('cache_root');

		if( !$dirhandle = @opendir($directory) )
			return;

		while( false !== ($filename = readdir($dirhandle)) ) {
			if( $filename != ".htaccess" && $filename != "." && $filename != "..") {
				$filename = $directory. "/". $filename;
				if( @filemtime($filename) < (time()-$seconds_old) )
					@unlink($filename);
			}
		}
	}

	public static function unlinkCached($cache) {
		if( strpos($cache,'.')!==FALSE || strpos($cache,'htaccess')!==FALSE)
			return;
		if( strpos($cache,'/')!==FALSE || strpos($cache,'\\')!==FALSE )
			return;

		$directory = \App::getDir('cache_root');
		if( !$dirhandle = @opendir($directory) )
			return;

		while( false !== ($filename = readdir($dirhandle)) ) {
			if( strpos($filename,$cache)!==FALSE && $filename != "." && $filename != "..") {
				@unlink($directory. "/". $filename);
			}
		}
	}

	public static function dropCache()
	{
	    $cache_dir = \App::getDir('cache_root');
	    if( is_dir($cache_dir) && $dirhandle = opendir($cache_dir) ) {
	        while( false !== ($filename = readdir($dirhandle)) ) {
	            if( $filename == "." || $filename == ".." ) continue;
	            self::recursiveDelete( "$cache_dir/$filename" );
	        }
	    }
	}

	private static function recursiveDelete( $dir )
	{
	    if ( is_dir($dir) && $dirhandle = opendir($dir) ) {
	        while( false !== ($filename = readdir($dirhandle)) ) {
	            if( $filename == "." || $filename == ".." ) continue;
	            if ( is_dir("$dir/$filename") ) {
	                self::recursiveDelete("$dir/$filename");
	            } else {
	                unlink("$dir/$filename");
	            }
	        }
	        rmdir("$dir");
	    } elseif( !is_dir($dir) ) {
            unlink("$dir");
	    }
	}

	public static function cacheSize()
	{
	    return self::directorySize( \App::getDir('cache_root') );
	}

	public static function directorySize( $dir )
	{
	    if( $dir == "." || $dir == ".." ) return 0;
	    if ( is_dir($dir) && $dirhandle = opendir($dir) ) {
	        $size = 0;
	        while( false !== ($filename = readdir($dirhandle)) ) {
	            if( substr($filename, 0, 1) ==  "." ) continue;
	            if ( is_dir("$dir/$filename") ) {
	                $size += self::directorySize("$dir/$filename");
	            } else {
	                $size += filesize("$dir/$filename");
	            }
	        }
	        return $size;
	    } elseif( !is_dir($dir) ) {
	        return filesize("$dir/$filename" );
	    }

	    return 0;
	}
}