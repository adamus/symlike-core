<?php
namespace Core\Kernel\Driver;

use Core\Kernel\Driver\CacheController;

/**
 * Cache layer between queries and the database.
 *  - store the results of large-scale queries on the file-system
 *  - memory cache of small-scale for current runtime
 *
 * @author Ádám Török (adamus.tork@gmail.com)
 */
class DatabaseCache
{
    protected $shared_cache = array();

	protected $db = null;
	protected $limit = 0;

	protected $cache_dir = '';

	/**
	 * @param \Core\Kernel\Driver\DatabaseConnection $db
	 * @param integer $limit
	 */
	function __construct($db, $limit)
	{
		$this->db = $db;
		$this->limit = $limit;


		$this->cache_dir = \App::getDir('database_cache');
		if (!is_dir($this->cache_dir)) {
			mkdir($this->cache_dir, 0777, true);
		}
	}

	function __destruct()
	{
	    foreach( $this->shared_cache as $i => &$obj ) {
	        $obj = 0;
	    }
	}

	public function setLimit( $limit )
	{
	    $this->limit = $limit;
	}

	/**
	 * Overloading this object to act as DatabaseConnection.
	 *
	 * @param string $method
	 * @param array $arguments
	 */
	public function &__call($method, $arguments)
	{
	    // Create the uniq hash string for the query:
		$hash = $this->limit > 1 ? md5($method . json_encode( $this->clearTimer($arguments) )) : md5($method . json_encode($arguments) );

		if( !isset($this->shared_cache[ $hash ]) ) {
		    if( $this->limit > 1 ) {
		        $query = $this->cache_dir . DIRECTORY_SEPARATOR . 'query_' . $hash;
		        $cache = CacheController::retreive($query);

		        if ( !($cache instanceof \Core\Kernel\Entity\CachedQuery) || $cache->cachedDataIsExpired() ) {
		            $result = $this->callDatabase($method, $arguments);
		            if( !empty($result) ) {
		                $cache = new \Core\Kernel\Entity\CachedQuery( $query, $result, $this->limit );
		            } else {
		                $cache = false;
		            }
		        }
		        $this->shared_cache[ $hash ] = $cache;

		    } else {
		        $result = $this->callDatabase($method, $arguments);
		        $this->shared_cache[ $hash ] = $result;
		    }
		}

		if( !substr($method, -6) != 'object' && $this->shared_cache[ $hash ] instanceof \Core\Kernel\Entity\CachedQuery ) {
		    return $this->shared_cache[ $hash ]->cachedDataReference();
		} else {
		    return $this->shared_cache[ $hash ];
		}
	}

	/**
	 * Pass call to the DatabaseConnection.
	 *
	 * @param string $method
	 * @param array $arguments
	 * @return mixed
	 */
	protected function callDatabase($method, $arguments)
	{
        return call_user_func_array(
		    array(
		        $this->db,
		        $method
		    ),
		    $arguments
        );
	}

	/**
	 * Remove the current timestamps from the query.
	 *
	 * @param array $array
	 * @return array
	 */
	protected function clearTimer($array) {
		if (!is_array($array)) return array();

		foreach ($array as $key=>&$value) {
			 if (is_array($value)) {
			 	$this->clearTimer($value);
			 } else {
			 	if ( is_int($value) && $value == time()) {
			 		$value=0;
			 	}
			 }
		}

		return $array;
	}

}
