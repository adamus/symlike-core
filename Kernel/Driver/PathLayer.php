<?php
namespace Core\Kernel\Driver;

use Core\Kernel\App;

//$String = preg_replace('/(?<!\ )[A-Z]/', '\\\$0', $String);
class PathLayer
{

    public static function parseFilePath( $alias )
    {
        $a = explode( ':', $alias );

        if ( count($a) == 1 ) {
            return App::getDocumentRoot() . 'app/' . $a[0];

        } else {
            $a[0] = preg_replace('/(?<!\ )[A-Z]/', '/$0', $a[0]);
            return App::getDocumentRoot() .'modules' . implode( '/', $a );

        }
    }

    public static function parseResponsePath( $alias )
    {
        $a = explode( '.', $alias ); // Module.Submodule.Class.Method
        return array( "\\{$a[0]}\\{$a[1]}\\Response\\{$a[2]}Response" , $a[3] );
    }

    public static function parseTranslationPath( $alias, $lang = 'hu' )
    {
        $trans_loc = explode( ':', $alias );
        $trans_type = explode( '.', $trans_loc[1]);
        if( !isset($trans_type[1]) ) $trans_type[1] = 'json';
        return App::getDocumentRoot() . 'modules' . preg_replace('/(?<!\ )[A-Z]/', '/$0', $trans_loc[0]) . "/Resources/Translations/{$trans_type[0]}_{$lang}.{$trans_type[1]}";
    }

    public static function parseClassPath( $alias )
    {
        return preg_replace('/(?<!\ )[A-Z]/', '\\\$0', $alias);
    }

    public static function pathReplace( $path, $params = array() ) {
        $preg = preg_match_all('/{\w+}/', $path, $matches);
        if ($preg) {
            while(count($matches[0])&&count($params)) {
                $path = str_replace(array_shift($matches[0]), array_shift($params), $path);
            }
        }
        return $path;
    }

    public static function loadEscapeJSONFile( $path )
    {
        $contents = file_get_contents( $path );
        preg_match_all('/"(.*?)"/im', $contents, $matches);
        foreach( $matches[1] as $match ) {
            $contents = str_replace( '"' . $match . '"', json_encode($match), $contents );
        }
        return json_decode( $contents );
    }

}