<?php
namespace Core\Kernel\Driver;

/**
 * PDO-based Database layer
 *
 * @author Ádám Török (adamus.tork@gmail.com)
 */
class DatabaseConnection
{
	protected $link;
	protected $debug = false;

	function __construct($mysql_host,$mysql_user,$mysql_pass,$mysql_db)
	{
		try {
			$this->link = new \PDO("mysql:dbname=$mysql_db;host=$mysql_host", $mysql_user, $mysql_pass);
		} catch (\PDOException $e) {
			die('MySQL Connection failed: ' . $e->getMessage());
		}
	}

	public function setDebugOn()
	{
	    $this->debug = true;
	    $this->link->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
	}

	public function setDebugOff()
	{
	    $this->debug = false;
	    $this->link->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_SILENT);
	}

	/**
	 * Insert into a table.
	 *
	 * @param string $table Name of the table.
	 * @param array $data Input assoc array.
	 * @param array|string|NULL $dupl Column names to update on duplicate key.
	 *
	 * @return integer|boolean
	 */
	function insert($table,$data,$dupl=null)
	{
		$IGNORE = '';
		$ONDUPLICATE = '';
		$ROWS = '';
		$VALUES = '';
		$INSERT = array();

		if ($dupl==='IGNORE') {
			$IGNORE = ' IGNORE';
		} elseif(is_array($dupl)) {
			$ONDUPLICATE = ' ON DUPLICATE KEY UPDATE ';
			foreach($dupl as $key) {
				$ONDUPLICATE.=" `$key`=VALUES(`$key`),";
			}
			$ONDUPLICATE=rtrim($ONDUPLICATE,',').';';
		}

		foreach ($data as $key=>$val) {
			$ROWS.="`$key`,";
			$VALUES.=":$key,";
			$INSERT[":$key"]=$val;
		}
		$ROWS=rtrim($ROWS,',');
		$VALUES=rtrim($VALUES,',');

		$SQL = "INSERT$IGNORE INTO $table ($ROWS) VALUES ($VALUES)$ONDUPLICATE";
		if( $this->debug ) \App::log( "INSERT$IGNORE INTO $table ($ROWS) VALUES (".$this->prepare_test($VALUES, $INSERT).")$ONDUPLICATE",'sql');

		try {
		    $prep = $this->link->prepare($SQL,array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
			$success = $prep->execute($INSERT);

		} catch (\PDOException $err) {
			$this->exceptionTrace( $err, $this->prepare_test($SQL, $INSERT) );
		}

		if ($success) {
			return $this->link->lastInsertId();
		} else {
			return false;
		}
	}

	/**
	 * Update a table
	 *
	 * @param string $table Name of the table.
	 * @param array $data Input assoc array - cols to update.
	 * @param string $WHERE Clause where to update.
	 * @param array|boolean $PARAMS where clause's parameters.
	 *
	 * @return boolean
	 */
	function update($table,$data,$WHERE,$PARAMS=FALSE)
	{
		$SET = '';
		$UPDATE = array();
		foreach ($data as $key=>$val) {
			$UPDATE[":$key"]=$val;
			$SET.="`$key`=:$key,";
		}
		$SET=rtrim($SET,',');
		if (is_array($PARAMS)) {
			foreach ($PARAMS as $pKey=>$pVal) {
				$UPDATE[":$pKey"]=$pVal;
			}
		}

		$SQL = "UPDATE $table SET $SET WHERE $WHERE";

		try {
		    $prep = $this->link->prepare($SQL,array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
		    $result = $prep->execute($UPDATE);
		    return $result;
        } catch (\PDOException $err) {
            $this->exceptionTrace( $err, $this->prepare_test($SQL, $PARAMS) );
        }

        return false;
	}

	/**
	 * Fetch single row.
	 *
	 * @param string $SQL Statement
	 * @param array|boolean $PARAMS Parameter(s)
	 *
	 * @return array|boolean
	 */
	function fetch_first($SQL,$PARAMS=FALSE)
	{
		if (substr(rtrim($SQL,';'),-7) != 'LIMIT 1') $SQL=rtrim($SQL,';').' LIMIT 1;';
		if( $this->debug ) \App::log( $this->prepare_test($SQL,$PARAMS),'sql');
		if (FALSE !== $this->prepare_execute($SQL,$PARAMS,$prep)) {
			return $prep->fetch(\PDO::FETCH_ASSOC);
		}

		return FALSE;
	}


	/**
	 * Fetch multiple rows
	 *
	 * @param string $SQL Statement
	 * @param array|boolean $PARAMS Parameter(s)
	 *
	 * @return array|boolean
	 */
	function fetch_all($SQL,$PARAMS=FALSE)
	{
		if( $this->debug ) \App::log( $this->prepare_test($SQL,$PARAMS),'sql');
		if (FALSE !== $this->prepare_execute($SQL,$PARAMS,$prep)) {
			return $prep->fetchAll(\PDO::FETCH_ASSOC);
		}
		return FALSE;
	}


	/**
	 * Fetch single object.
	 *
	 * @param string|Object $OBJECT Name or instance of the object class.
	 * @param string $SQL Statement
	 * @param array|boolean $PARAMS Parameter(s) to fetch the query.
	 * @param array|boolean $CONSTRUCT Parameter(s) to construct the object.
	 *
	 * @return object|boolean
	 */
	function fetch_object($OBJECT,$SQL,$PARAMS=FALSE,$CONSTRUCT=NULL)
	{
		if (substr(rtrim($SQL,';'),-7) != 'LIMIT 1') $SQL=rtrim($SQL,';').' LIMIT 1;';
		try {
		    if (false !== $this->prepare_object_query($OBJECT,$SQL,$PARAMS,$CONSTRUCT,$prep) ) {
		        $result = $prep->fetch();
		        $prep->closeCursor();
		        return $result;
		    }
        } catch (\PDOException $err) {
            $this->exceptionTrace( $err, $this->prepare_test($SQL, $PARAMS) );
        }
		return false;
	}

	/**
	 * Fetch multiple objects.
	 *
	 * @param string|Object $OBJECT Name or instance of the object class.
	 * @param string $SQL Statement
	 * @param array|boolean $PARAMS Parameter(s) to fetch the query.
	 * @param array|boolean $CONSTRUCT Parameter(s) to construct the objects.
	 *
	 * @return array|boolean
	 */
	function fetch_all_object($OBJECT,$SQL,$PARAMS=FALSE,$CONSTRUCT=null)
	{
	    if (false !== $this->prepare_object_query($OBJECT,$SQL,$PARAMS,$CONSTRUCT,$prep) ) {
	        $result = array();
	        while ($object = $prep->fetch()) {
	            $result[] = clone $object;
	        }
	        $prep->closeCursor();
	        return $result;
	    }
        return false;
	}


	/**
	 * Single execution.
	 *
	 * @param string $SQL Statement
	 * @param array|boolean $PARAMS Parameter(s)
	 *
	 * @return boolean
	 */
	function exec($SQL,$PARAMS=FALSE)
	{
	    return FALSE !== $this->prepare_execute($SQL,$PARAMS,$prep);
	}

	protected function prepare_execute($SQL,$PARAMS,&$prep)
	{
	    if( $this->debug ) \App::log($this->prepare_test($SQL, $PARAMS),'sql');
	    try {
	        $prep = $this->link->prepare($SQL);

	        if (!is_array($PARAMS)) {
	            return $prep->execute();
	        } else {
	            return $prep->execute($PARAMS);
	        }

		} catch (\PDOException $err) {
		    $this->exceptionTrace( $err, $this->prepare_test($SQL, $PARAMS) );
		}
		return false;
	}

	protected function exceptionTrace( $err, $sql )
	{
		\App::log(  $err->getMessage() . ' (' . $err->getCode() . ') ' . $err->getFile() . ' @ line ' .$err->getLine() . "\n" . $sql . "\n\n", 'sql');
	}

	protected function prepare_object_query($OBJECT,$SQL,$PARAMS,$CONSTRUCT,&$prep)
	{
	    if( $this->debug ) \App::log($this->prepare_test($SQL, $PARAMS),'sql');
		try {
		    $prep = $this->link->prepare($SQL);

			if (is_object($OBJECT)) {
				$prep->setFetchMode(\PDO::FETCH_INTO,$OBJECT);
			} else {
				if (empty($CONSTRUCT)) {
					$prep->setFetchMode(\PDO::FETCH_CLASS,$OBJECT);
				} else {
					$prep->setFetchMode(\PDO::FETCH_CLASS,$OBJECT,$CONSTRUCT);
				}
			}

		} catch (\PDOException $err) {
			$this->exceptionTrace( $err, $this->prepare_test($SQL, $PARAMS) );
		}

		try {
			if (is_array($PARAMS)) {
				$prep->execute($PARAMS);
			} else {
				$prep->execute();
			}
		} catch (\PDOException $err) {
			$this->exceptionTrace( $err, $this->prepare_test($SQL, $PARAMS) );
		}
	}

	public function prepare_test($SQL,$PARAMS=false)
	{
		$test = $SQL;
		if(is_array($PARAMS)) {
			foreach($PARAMS as $key=>$val) {
				if(is_numeric($val)) $test = str_replace(":$key",$val,$test);
				elseif(is_null($val)) $test = str_replace(":$key",'NULL',$test);
				else $test = str_replace(":$key",$this->link->quote($val),$test);
			}
		}
        return $test;
	}
}
