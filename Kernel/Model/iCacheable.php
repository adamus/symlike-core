<?php
namespace Core\Kernel\Model;

interface iCacheable
{
    /**
     * What to do if the cached data is out of date.
     */
    function onCacheLimit();

    /**
     * What to do when the unserialize event occours.
     */
    function __wakeup();

    /**
     * What to do when the serialize event occours.
     * -> Must return an array of stored properties.
     */
    function __sleep();

}