<?php
namespace Core\Kernel\Model;

use Core\Kernel\Model\Collection;

/**
 * Collects one or more rows from a table.
 *
 * @author Ádám Török (adamus.tork@gmail.com)
 */
class Collector extends Collection
{
    /**
     * @param string $table Name of the table
     * @param array $fetch Fetch conditions
     * @param string $model Class type
     * @param string $groupping MySQL GROUP BY column statement
     * @param string $ordering MySQL ORDER BY column statement
     * @param integer $querycache Reload after the defined minutes
     */
    function __construct( $table, $fetch, $model = '\\stdClass', $groupping = '', $ordering = '', $querycache = 1 )
    {
        $this->_table = $table;
        $this->_model = $model;
        $this->_groupping = $groupping;
        $this->_ordering = $ordering;

        parent::__construct( $fetch, $querycache );
    }

    /**
     * Overloaded calls.
     *     ->findById( $id ) === ->findBy( 'Id', $id )
     */
    function __call( $name, $arguments )
    {
        if ( strtolower( substr( $name, 0, strlen('collectby') ) ) == 'collectby' ) {
            return $this->collectBy( strtolower( substr( $name, strlen('collectby') ) ) );

        } elseif ( strtolower( substr( $name, 0, strlen('findby') ) ) == 'findby' && !empty($arguments) ) {
            return $this->findBy(strtolower( substr( $name, strlen('findby') ) ), $arguments[0] );

        }

        return null;
    }

    /**
     * Collects to rows into an array with the given key property / method
     * @param string $key
     * @return array
     */
    public function collectBy( $key )
    {
        if ( ($method = $this->hasMethod( $key )) != null ) {
            $a = array();
            foreach( $this->_rows as $row ) $a[ $row->$method() ] = $row;
            return $a;

        } elseif( ($property = $this->hasPublic( $key )) != null ) {
            $a = array();
            foreach( $this->_rows as $row ) $a[ $row->$property ] = $row;
            return $a;

        }

        return null;
    }

    /**
     * Returns ONE row with the specified key-value pair.
     * @param string $key
     * @param string|integer $value
     * @return \Core\Kernel\Model\Entity
     */
    public function findBy( $key, $value )
    {
        if ( ($i = $this->findRowKeyBy($key, $value)) !== null )
            return $this->_rows[$i];

        return null;
    }

    public function findRowKeyBy( $key, $value )
    {
        if ( ($method = $this->hasMethod( $key )) != null ) {
            foreach( $this->_rows as $i=>$row )
                if( $row->$method() == $value )
                    return $i;

        } elseif( ($property = $this->hasPublic( $key )) != null ) {
            foreach( $this->_rows as $i=>$row ) {
                $row = (object)$row;
                if( isset($row->$property) ) {
                    if( $row->$property == $value )
                        return $i;
                } else {
                    ob_start();
                    var_dump($this);
                    \App::log( "no $property\n" . ob_get_clean() . "\n\n", 'collector-bug' );
                }
            }

        }

        return null;
    }

    public function updateRowByKey( $key, $name, $new_value )
    {
        if( ($i = $this->findRowKeyBy($key, $name)) !== null ) {
            $this->_rows[$i] = $new_value;

            if( is_array($new_value) or $new_value instanceof \stdClass  ) {
                $this->storeRow( (array)$new_value );
                return true;
            } elseif( $new_value instanceof \Core\Kernel\Model\Entity ) {
                $new_value->storeRow();
                return true;
            }
        }
        return false;
    }

    public function deleteBy( $key, $value )
    {
        if ( ($i = $this->findRowKeyBy($key, $value)) !== null ) {
            if( $this->_rows[$i] instanceof \Core\Kernel\Model\Entity ) {
                $this->_rows[$i]->deleteRow();
            }
            unset( $this->_rows[$i] );
        }
    }

    /**
     * Test if the rows has the given type of method.
     * @param string $key
     * @return null|string
     */
    protected function hasMethod( $key )
    {
        if ( !isset( $this->_rows[0] ) || !is_object( $this->_rows[0] ) ) return null;
        foreach( get_class_methods($this->_rows[0]) as $method ) {
            if ( $this->toCommon( $method ) == 'get'.$this->toCommon( $key ) ) {
                return $method;
            }
        }

        return null;
    }

    /**
     * Tests if the rows has the given type of property.
     * @param string $key
     * @return null|string
     */
    protected function hasPublic( $key )
    {
        if ( !isset( $this->_rows[0] ) || !is_object( $this->_rows[0] ) ) return null;
        foreach( get_object_vars($this->_rows[0]) as $property => $value) {
            if ( $this->toCommon( $property ) == $this->toCommon( $key ) ) {
                return $property;
            }
        }

        return null;
    }

    protected function toCommon( $string )
    {
        return strtolower( preg_replace( '/[^\w\d]+/', '', $string ) );
    }

    /**
     * @param \Core\Kernel\Model\Entity $entity
     * @return \Core\Kernel\Model\Collector
     */
    public function addEntity( \Core\Kernel\Model\Entity $entity )
    {
        $this->_rows[]= $entity;
        return $this;
    }

    /**
     * @param array|stdClass $row
     */
    public function addRow( $row )
    {
        $this->storeRow( (array)$row );
        $this->_rows[]= $row;
        return $this;
    }
}
