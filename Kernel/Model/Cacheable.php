<?php
namespace Core\Kernel\Model;

use Core\Kernel\Model\iCacheable;

abstract class Cacheable implements iCacheable
{

	/**
	 * Is really cacheable?
	 */
	protected $_cacheable = true;

	/**
	 * Cached at ~ timestamp
	 * @var integer $_cached
	 */
	protected $_cached = 0;

	/**
	 * Max cached time in seconds before reload.
	 * 		(Default = no reload).
	 * @var integer $_maxcache
	 */
	protected $_maxcache = -1;

	/**
	 * on serialize event
	 */
	function __sleep() {
		if ($this->_cacheable) {

			if ($this->_cached == 0) {
				$this->_cached = time();
			}

			return array_keys(get_object_vars($this));

		} else {
			return array();

		}
	}

	/**
	 * on unserialize event
	 */
	function __wakeup() {
		if ( $this->cachedDataIsExpired() ) {
			$this->onCacheLimit();
		}
	}

	public function getCached($formatted = true) {
	    if ($formatted) {
	        return date('Y-m-d H:i:s', (int)$this->_cached);
	    }
	    return (int)$this->_cached;
	}

    public function cachedDataStoreable()
    {
        return $this->_maxcache > 1 && $this->_cacheable;
    }

    public function cachedDataIsExpired()
    {
        return $this->cachedDataStoreable() && $this->_maxcache + $this->_cached < time();
    }

}