<?php
namespace Core\Kernel\Model;

use Core\Kernel\Model\Cacheable;

/**
 * Az adatbázisból egy tábla sorát képviseli, mint objektum.
 * 		* Ahol használva van, ott meg kell határozni a tábla adatait: tábla neve, és az sor azonosító (egyedi) kulcsa(i).
 * 		* Cachelhető (látogató / session szemszögből): alapértelmezetten 20 perc után tölti újra.
 *
 * @author Török Ádám (adamus.tork@gmail.com)
 */
abstract class Entity extends Cacheable {

    public static function getEntityTable() {
        return '';
    }

    /**
     * A táblából milyen oszlopokat válasszon ki.
     * @var string
     */
    protected $_select = '*';

    public function getEntityCols() {
        $schema = \App::getSchema()->describe( $this->getEntityTable() );
        return array_keys($schema['col']);
    }

    public function getEntityKey() {
        $schema = \App::getSchema()->describe( $this->getEntityTable() );
        return isset($schema['index']['PRIMARY']['columns'][0]) ? $schema['index']['PRIMARY']['columns'][0] : $schema['key'];
    }

    /**
     * Az SQL lekérést hány percre gyorsítótárazza.
     * @var integer $_querycache
     */
    protected $_querycache = 1;

    /**
     * Destructor mentsen-e
     * @var boolean $_save
     */
    protected $_save = false;

    public function getEntitySave() {
        return $this->_save;
    }

    public function setEntitySave($save) {
        $this->_save = $save;
    }

    #################
    # CONSTRUCT
    # 	& DESTRUCT

    /**
     * Ha megvan adva a konstruktornak az objektumpéldány egyedi azonosítója, akkor betölti az adatbázisból.
     * @param array $construct
     */
    function __construct($fetch = false) {
		try {
			if ($this->getEntityTable() != '') {
				$this->getEntityCols();

				if (is_array($fetch)) {
					$result = $this->queryRow($fetch);
					if (is_array($result)) {
						$this->loadRow($result);
					} else {
						/*
						 * Ha nincs találat az adatbázisban, akkor vissza ad egy objektumot a keresési értékekkel.
						* Ezért nem szabadna egy ilyen objektumot a keresési értékek alapján vizsgálni, hogy valós-e!
						* Már több mindent erre építettem, úgyhogy a visszavonása katasztrófát okoz! :)
						*/
						$this->loadRow($fetch);
					}
				}
			}
		} catch( \Exception $e ) {
			$this->loadRow($fetch);
		}
    }

    /**
     *  A destruktort is menthet (nem cachelt objektumoknál).
     */
    function __destruct() {
        if ($this->_save && $this->testUniqueKey()) {
            $this->storeRow();
        }
    }
    /**
     * És a serialize() is menthet: mert a destruktor hiába teszi a _save-et false-ra, a sleep-el már belement a true ;)
     * @see db_cache::__sleep()
     */
    function __sleep() {
        if ($this->_save && $this->testUniqueKey()) {
            $this->storeRow();
        }
        return parent::__sleep();
    }

    #################
    # IMPLEMENT CACHE

    function onCacheLimit() {
        $this->_cached = time();
        $this->reloadRow();
    }

    #################
    # STORAGE

    public function toArray() {
        $export = array();
        foreach($this->getEntityCols() as $col) {
            if( method_exists($this, "get{$col}") ) {
                $export[$col] = $this->{"get{$col}"}();
            }
        }
        return $export;
    }

    /**
     * Lekérést végez a megadott oszlop=>érték megfeleltetés alapján.
     * @param array $query
     * @return array[]
     */
    protected function queryRow($query) {
        $table = $this->getEntityTable();
        $where = $this->createClauseByAssoc($query);
        return \App::getDB($this->_querycache)
                ->fetch_first(
                        "SELECT {$this->_select} FROM {$table} WHERE ${where['clause']} LIMIT 1;",
                        $where['params']);
    }

    /**
     * Feltölti az objektumot.
     * @param array $row
     */
    public function loadRow(&$row) {
        if (is_array($row)) {
            foreach ($row as $key => &$value) {
                $this->$key = &$value;
            }
        }
    }

    /**
     * Újratölti az objektumot a kulcsok alapján.
     */
    protected function reloadRow() {
        $key = $this->getEntityKey();
        if (!empty($key) && $this->getEntityTable() != '') {
            $table = $this->getEntityTable();
            $where = $this->createClauseByCurrent($key);
            $query = \App::getDB()
                    ->fetch_first(
                            "SELECT * FROM {$table} WHERE ${where['clause']} LIMIT 1;",
                            $where['params']);
            $this->loadRow($query);
            $this->_cached = time();
        }
    }

    /**
     * Elmenti az objektumot az implementáláskor megadott táblába.
     */
    public function storeRow() {
        if ($this->getEntityTable() != '') {
            $this->_save = false; //ne mentsünk duplán
            $insert = $this->createStoredData();
            return \App::getDB()
                    ->insert($this->getEntityTable(), $insert,
                            $this->updateOnDuplicate(array_keys($insert)));
        }
        return false;
    }

    /**
     * Törli a tábla sorát.
     */
    public function deleteRow() {
        $this->_save = false;

        $key = $this->getEntityKey();
        if (!empty($key) && $this->getEntityTable() != '') {
            $table = $this->getEntityTable();
            $where = $this->createClauseByCurrent($key);
            \App::getDB()
                    ->exec($s = "DELETE FROM {$table} WHERE ${where['clause']}",
                            $where['params']);
        }
    }

    #################
    # PREPARE

    /**
     * Előkészíti az SQL lekérés feltételét a kulcsokból.
     * @param array|string $key
     * @param boolean $self
     * @param string $prefix
     * @return array[clause,params]
     */
    protected function createClauseByCurrent($key) {
        $params = array();

        if (!is_array($key)) {
            $clause = $key . '=?';
            $params[] = $this->$key;

        } else {
            $clause = implode('=? AND ', $key) . '=?';
            foreach ($key as $param) {
                if( property_exists($this, $param) ) $params[] = $this->$param;
            }
        }

        return array('clause' => $clause, 'params' => $params);
    }

    protected function createClauseByAssoc($array, $prefix = '') {
        $params = array();
        $clause = $prefix . implode('=? AND ' . $prefix, array_keys($array))
                . '=?';
        foreach ($array as $param) {
            $params[] = $param;
        }
        return array('clause' => $clause, 'params' => $params);
    }

    /**
     * Elkészíti a frissítendő oszlopokat, ha duplikáció lép fel a mentéskor.
     * @param array $update Az összes oszlop.
     * @return array A frissítendő oszlopok.
     */
    protected function updateOnDuplicate($update) {
        $duplicate_key = $this->getEntityKey();

        if ($duplicate_key !== null) {
            if (!is_array($duplicate_key)) {
                $duplicate_key = array($duplicate_key);
            }

            foreach ($duplicate_key as $out) {
                if (($pos = array_search($out, $update)) !== FALSE) {
                    unset($update[$pos]);
                }
            }
        }

        return $update;
    }

    /**
     * Ellenőrzi az elsődleges kulcso(ka)t.
     * @return boolean
     */
    protected function testUniqueKey() {
        $primary = $this->getEntityKey();

        if ($primary !== null) {
            if (!is_array($primary)) {
                $primary = array($primary);
            }

            foreach ($primary as $test) {
                if (empty($this->$test)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Összeállítja az eltárolandó sort a táblába.
     * @return array
     */
    protected function createStoredData() {
        $store = array();
        $cols = $this->getEntityCols();

        foreach ($cols as $col) {
            if (property_exists($this, $col)) {
                $store[$col] = $this->$col;
            }
        }

        return $store;
    }

    public function cloneCols($from) {
        foreach ($this->getEntityCols() as $col) {
            $this->{"set{$col}"}($from->{"get{$col}"}());
        }
    }

    /**
     *
     * @param array $fetch
     * @param string $group_by
     * @param string $order_by
     * @param integer $querycache
     *
     * @return \Core\Kernel\Model\Collector
     */
    public static function collector($fetch, $group_by = '', $order_by = '', $querycache = 1) {
        $called = get_called_class();
        $collector = new \Core\Kernel\Model\Collector($called::getEntityTable(), $fetch, $called, $group_by, $order_by, $querycache);
        return $collector;
    }

    /**
     * @return \Core\Kernel\Provider\CollectionQueryBuilder
     */
    public static function collectionQueryBuilder()
    {
        $called = get_called_class();
        return \Core\Kernel\Provider\CollectionQueryBuilder::builder( $called );
    }
}
