<?php
namespace Core\Kernel\Model;

use Core\Kernel\Model\Cacheable;

/**
 * Absztrakt osztály ami az adatbázisból egy tábla több sorát képviseli, mint objektum.
 *
 * @author Török Ádám (adamus.tork@gmail.com)
 */
abstract class Collection extends Cacheable {

	/**
	 * A tábla neve ahonnan costructor tölt / ahova a destructor ment.
	 * @var string $_table
	 */
	protected $_table = '';

	/**
	 * A tábla egyedi azonosító kulcsa(i).
	 * @var array $_key
	 */
	protected $_key = array();

	/**
	 * Mi alapján töltött be.
	 * @var array $_fetch
	 */
	protected $_fetch = array();
	protected $_select = '*';
	protected $_groupping = '';
	protected $_ordering  = '';

	/**
	 * Az SQL lekérést hány percre gyorsítótárazza.
	 * @var integer $_querycache
	 */
	protected $_querycache = 1;

	/**
	 * A kezelt sorok.
	 * @var array[\Core\Kernel\Model\Entity] $_rows
	 */
	protected $_rows = array();

	/**
	 * @return array[\Core\Kernel\Model\Entity]
	 */
	public function getAll()
	{
	    return $this->_rows;
	}

	/**
	 * Az osztályok típusa, amiket betöltsön.
	 * @var string $_model
	 */
	protected $_model = '\\stdClass';


	function __construct( $fetch = false, $querycache = 1 ) {
	    $this->_querycache = $querycache;

		if ( !empty($this->_table) ) {
			$this->loadRowKeys();

			if( !empty($fetch) ) {
			    $this->_fetch = $fetch;
			    $this->loadRows($this->_fetch);
			}
		}
	}

	protected function loadRowKeys()
	{
	    $schema = \App::getSchema()->describe($this->_table);
	    return $schema['key'];
	}

	public function loadRows( $fetch )
	{
	    $this->_rows = array();

	    if( is_array($fetch) && !empty($fetch) ) {
	        if( isset($fetch[0]) && isset($fetch[1]) && !is_array($fetch[0]) && is_array($fetch[1]) ) {
	            $this->_rows = \App::getDB($this->_querycache)->fetch_all_object($this->_model, $s="SELECT {$this->_select} FROM {$this->_table} WHERE {$fetch[0]} {$this->_groupping} {$this->_ordering};", $fetch[1]);

	        } else {
	            $where = $this->createClauseByAssoc($fetch);
	            $this->_rows = \App::getDB($this->_querycache)->fetch_all_object($this->_model, "SELECT {$this->_select} FROM {$this->_table} WHERE ${where['clause']} {$this->_groupping} {$this->_ordering};", $where['params']);
	        }

	    } elseif( !empty($fetch) ) {
	        $this->_rows = \App::getDB($this->_querycache)->fetch_all_object($this->_model, "SELECT {$this->_select} FROM {$this->_table} WHERE {$fetch} {$this->_groupping} {$this->_ordering};");
	    }

	    if( $this->_rows instanceof \Core\Kernel\Entity\CachedQuery ) {
	        $this->_rows = &$this->_rows->cachedDataReference();
	    }

	    return @count($this->_rows);
	}

	public function reloadRows()
	{
	    return $this->loadRows($this->_fetch);
	}

	public function storeRow($row)
	{
		if ( !empty($this->_table) ) {
			return \App::getDB()->insert($this->_table, $row, $this->updateOnDuplicate(array_keys($row)));
		}

		return false;
	}

	public function deleteAllRow() {
	    if( is_array($this->_fetch) ) {
	        if( isset($fetch[0]) && isset($fetch[1]) && !is_array($fetch[0]) && is_array($fetch[1]) ) {
	            \App::getDB()->exec("DELETE FROM {$this->_table} WHERE {$this->_fetch}", $fetch[1]);
	        } else {
	            $where = $this->createClauseByAssoc($this->_fetch);
	            \App::getDB()->exec("DELETE FROM {$this->_table} WHERE ${where['clause']}", $where['params']);
	        }
	    } else {
	        \App::getDB()->exec("DELETE FROM {$this->_table} WHERE {$this->_fetch}");
	    }
		$this->_rows=array();
		return $this;
	}

	protected function updateOnDuplicate($update) {
		$duplicate_key = $this->_key;

		if ($duplicate_key !== null) {
			if (!is_array($duplicate_key)) {
				$duplicate_key = array($duplicate_key);
			}

			foreach ($duplicate_key as $out) {
				if ( ($pos = array_search($out,$update)) !== FALSE ) {
					unset($update[$pos]);
				}
			}
		}

		return $update;
	}

	protected function createClauseByAssoc($array) {
		$params = array();
		$clause = implode('=? AND ', array_keys($array)) . '=?';
		foreach ($array as $param) {
			$params []= $param;
		}
		return array('clause'=>$clause,'params'=>$params);
	}

	protected function createClauseForRow($row) {
		$params = array();

		if (!is_array($this->_key)) {
			$clause = $this->_key . '=?';
			$params []= $row->$this->_key;

		} else {
			$clause = implode('=? AND ', $this->_key) . '=?';
			foreach ($this->_key as $param) {
				$params []= $row->$param;
			}
		}

		return array('clause'=>$clause,'params'=>$params);
	}


	function onCacheLimit() {
	    $this->_cached = time();
		if( !empty($this->_fetch) ) {
			$this->loadRows($this->_fetch);
		}
	}

	/**
	 * @param string $key Row key
	 * @return array[Row key's value][$row]
	 */
	public function toArray( $key = null ) {
	    $export = array();
	    $rows = $this->getAll();
	    if( is_array($rows) ) {
	        foreach($rows as $row) {

	            if( is_callable(array($row, 'toArray' )) ) { // Entity, Collection, Collector
	                if( is_callable(array($row, $method = "get{$key}" )) ) {
	                    $export[ $row->$method() ] = $row->toArray();
	                } else {
	                    $export[] = $row->toArray();
	                }
	            } else { // stdClass
	                if( isset($row->$key) ) {
	                    $export[ $row->$key ] = (array)$row;
	                } else {
	                    $export[] = (array)$row;
	                }
	            }
	        }
	    }
	    return $export;
	}

	public function isEmpty()
	{
	    return empty($this->_rows);
	}

	public function getRowsCount()
	{
        return count($this->_rows);
	}

}