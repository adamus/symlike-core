<?php
namespace Core\Kernel\Provider;

use Core\Kernel\Model\Collector;

class CollectionQueryBuilder
{
	protected $table = '';
	protected $model = 'stdClass';
	protected $groupping = '';
	protected $ordering = '';
	protected $construct_params = false;
	protected $fetch = false;
	protected $query_cache = 1;
	protected $select = '*';

	function __construct( $model, $table ) {
		$this->model = $model;
		$this->table = $table;
	}

	/**
	 * @return \Core\Kernel\Provider\CollectionQueryBuilder
	 */
	public static function builder( $model )
	{
		return new self( $model, $model::getEntityTable() );
	}

	/**
	 * @return \Core\Kernel\Model\Collector
	 */
	public function collect()
	{
		$collector = new Collector($this->table, $this->fetch, $this->model, $this->groupping, $this->ordering, $this->query_cache, $this->construct_params, $this->select );
        return $collector;
	}

	/**
	 * @return array[\Core\Kernel\Model\Entity]
	 */
	public function getAll()
	{
	    return $this->collect()->getAll();
	}

	/**
	 * @return array[\Core\Kernel\Model\Entity]
	 */
	public function collectBy( $key )
	{
	    return $this->collect()->collectBy( $key );
	}

	/**
	 * @return \Core\Kernel\Provider\CollectionQueryBuilder
	 */
	public function table( $table )
	{
		$this->table = $table;
		return $this;
	}
	/**
	 * @return \Core\Kernel\Provider\CollectionQueryBuilder
	 */
	public function groupBy( $groupping )
	{
		$this->groupping = $groupping;
		return $this;
	}
	/**
	 * @return \Core\Kernel\Provider\CollectionQueryBuilder
	 */
	public function orderBy( $ordering )
	{
		$this->ordering = $ordering;
		return $this;
	}
	public function construct( $construct_params )
	{
		$this->construct_params = $construct_params;
		return $this;
	}
	/**
	 * @return \Core\Kernel\Provider\CollectionQueryBuilder
	 */
	public function select( $select )
	{
	    $this->select = $select;
	    return $this;
	}
	/**
	 * @return \Core\Kernel\Provider\CollectionQueryBuilder
	 */
	public function query( $query )
	{
		$this->fetch = $query;
		return $this;
	}
	/**
	 * @return \Core\Kernel\Provider\CollectionQueryBuilder
	 */
	public function cache( $cache )
	{
		$this->query_cache = $cache;
		return $this;
	}

}