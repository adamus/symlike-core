<?php

	# HOST, URL
/**
 * Teljes GET/ URL + URI.
 * @return string
 */
function curPageURL() {
	$pageURL = 'http';
	if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
		$pageURL .= "s";
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

/**
 * Domain.
 * @return string
 */
function curServer() {
	$serverURL = 'http';
	if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
		$serverURL .= "s";
	}
	return $serverURL .= "://".$_SERVER["SERVER_NAME"];
}

function getBrowserLang($default = 'hu') {
	foreach( explode(',', @$_SERVER['HTTP_ACCEPT_LANGUAGE']) as $lang ) {
		$pattern = '/^(?P<primarytag>[a-zA-Z]{2,8})'.
				'(?:-(?P<subtag>[a-zA-Z]{2,8}))?(?:(?:;q=)'.
				'(?P<quantifier>\d\.\d))?$/';

		$splits = array();

		if (preg_match($pattern, $lang, $splits)) {
			if (isset($splits['primarytag'])) {
				return $splits['primarytag'];
			}
		}

	}

	return $default;
}

	# STRING - VALIDATOR
/**
 *
 * @param string $email
 * @return boolean
 */
function validEmail($email) {
	return filter_var($email, FILTER_VALIDATE_EMAIL) !== FALSE;
}

	# STRING - TICKERS
/**
 * Ticker írásjelek, szóköz, számok nélkül.
 * @param string $corpTicker
 * @return string
 */
function stripticker($corpTicker) {
	$return='';
	for($i=0;$i<strlen($corpTicker);$i++) {
		$char=substr($corpTicker,$i,1);
		$ord=ord($char);
		if($ord>64 && $ord<91)
			$return.=$char;
		else if($ord>96 && $ord<123)
			$return.=strtoupper($char);
	}
	return $return;
}

/**
 * Ticker írásjelek, szóköz nélkül.
 * @param string $corpTicker
 * @return string
 */
function tsticker($corpTicker) {
	$return='';
	for($i=0;$i<strlen($corpTicker);$i++) {
		$char=substr($corpTicker,$i,1);
		$ord=ord($char);
		if(($ord>64 && $ord<91)||($ord>47 && $ord<58))
			$return.=$char;
		else if($ord>96 && $ord<123)
			$return.=strtoupper($char);
	}
	return $return;
}

	# PAINT - COLORS
/**
 * Hexadecimal -> decimal
 * @param string $color
 * @return array[integer]
 */
function html2rgb($color) {
    if ($color[0] == '#')
        $color = substr($color, 1);

    if (strlen($color) == 6)
        list($r, $g, $b) = array($color[0].$color[1],
                                 $color[2].$color[3],
                                 $color[4].$color[5]);
    elseif (strlen($color) == 3)
        list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
    else
        return false;

    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);

    return array($r, $g, $b);
}

/**
 * Decimal -> hexadecimal
 * @param integer $r
 * @param integer $g
 * @param integer $b
 * @return string
 */
function rgb2html($r, $g=-1, $b=-1) {
    if (is_array($r) && sizeof($r) == 3)
        list($r, $g, $b) = $r;

    $r = intval($r); $g = intval($g);
    $b = intval($b);

    $r = dechex($r<0?0:($r>255?255:$r));
    $g = dechex($g<0?0:($g>255?255:$g));
    $b = dechex($b<0?0:($b>255?255:$b));

    $color = (strlen($r) < 2?'0':'').$r;
    $color .= (strlen($g) < 2?'0':'').$g;
    $color .= (strlen($b) < 2?'0':'').$b;
    return '#'.$color;
}

	# TIME
/**
 * Dátum string-et UTC időbélyeggé alapkít.
 * @param string $Stamp
 * @return integer
 */
function toTick($Stamp) {
	$tAr = date_parse($Stamp);
	return gmmktime($tAr['hour'],$tAr['minute'],$tAr['second'],$tAr['month'],$tAr['day'],$tAr['year']);
}
function stampsPassed($tStr) {
	$tAr = date_parse($tStr);
	$tStp = gmmktime($tAr['hour'],$tAr['minute'],$tAr['second'],$tAr['month'],$tAr['day'],$tAr['year']);
	return gmmktime()-$tStp;
}
function diffHour($apiStamp) {
	$apiTick = toTick($apiStamp);
	$localTick = time();
	return floor((180 + $localTick - $apiTick)/3600);
}
function diffMinute($apiStamp) {
	$apiTick = toTick($apiStamp);
	$localTick = time();
	return floor((180 + $localTick - $apiTick)/60);
}
function diffStamp($Stamp,$hour=0) {
	return diffTick(toTick($Stamp),$hour);
}
function diffTick($tick,$hour=0) {
	return toStampPart($tick - time(),$hour);
}
function toStampPart($tick,$hour=0) {
	$day = ($tick/60/60+$hour)/24;
	$hour = ($day-floor($day))*24;

	$r_d = floor($day);
	$r_h = floor($hour);

	$return = '';
	if($r_d>0) $return.= $r_d.'nap ';
	if($r_h>0) $return.= $r_h.'óra ';
	if($r_h<1 || $r_d<1) $return.=floor(($hour-$r_h)*60).'perc';

	return $return;
}
function hourStamp($hour) {
	$day = $hour/24;
	$hour = floor(($day-floor($day))*24);
	$day = floor($day);
	$return = $hour.'óra';
	if($day>0) $return = $day.'nap '.$return;
	return $return;
}

# BITWISE
function bat($a,$b) {		//Bit And Test
	return ($a&$b)==$b;
}
function bo($a,$b) {		//Bit Or
	return $a|$b;
}
function bor(&$a,$b) {		//Bit Or by Reference
	return $a=$a|$b;
}
function bna($a,$b) {		//Bit nAnd
	return $a&~$b;
}
function bnar(&$a,$b) {		//Bit nAnd by Reference
	return $a=$a&~$b;
}
?>