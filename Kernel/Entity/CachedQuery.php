<?php
namespace Core\Kernel\Entity;

use Core\Kernel\Driver\CacheController;
use Core\Kernel\Entity\SharedCache;

/**
 * Cache layer between the database query result and the stored object on the filesystem
 *    ~ attempting to hide the cache for the users with updating the object's changes.
 *
 * @author Ádám Török (adamus.tork@gmail.com)
 */
class CachedQuery extends SharedCache
{
    /**
     * @var string $_cache_path
     */
    protected $_cache_path = '';

    // -- overloading

    function __construct( $path, $entity, $until )
    {
        $this->_cache_path = $path;
        $this->_cached_data = $entity;
        $this->_cached = time();
        $this->_maxcache = $until;
    }

    function __destruct()
    {
        if( $this->cachedDataStoreable() && $this->cachedDataHasChanged() ) {
            $this->_cache_hash = $this->cachedDataHash();
            $this->_cached = time();
            CacheController::store($this->_cache_path, $this);
        }
    }

    function onCacheLimit()
    {
        return; //DatabaseCache has to handle reload
    }

    // -- methods


    public function &cachedDataReference()
    {
        return $this->_cached_data;
    }
}
