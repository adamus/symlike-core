<?php
namespace Core\Kernel\Entity;

use Core\Kernel\Model\Cacheable;
use Core\Kernel\Driver\CacheController;

abstract class SharedCache extends Cacheable
{
    protected $_cached_data = null;
    protected $_cache_hash = '';

    // -- overloading

    function &__get( $name )
    {
        $result = null;
        if( is_object($this->_cached_data) && property_exists($this->_cached_data, $name) ) {
            $result = &$this->_cached_data->$name;
        } elseif( is_array($this->_cached_data) && isset($this->_cached_data[$name]) ) {
            $result = &$this->_cached_data[$name];
        }
        return $result;
    }

    function __set( $name, $value )
    {
        if( is_object($this->_cached_data) ) $this->_cached_data->$name = $value;
        elseif( is_array($this->_cached_data) ) $this->_cached_data[$name] = $value;
    }

    function &__call( $name, $arguments )
    {
        $method = array($this->_cached_data, $name);
        $result = null;
        if( is_callable( $method ) ) {
            $result = call_user_func_array( $method, $arguments );
        }
        return $result;
    }

    function __destruct()
    {
        if( $this->cachedDataStoreable() && $this->cachedDataHasChanged() ) {
            $this->_cache_hash = $this->cachedDataHash();
            $this->_cached = time();
            CacheController::putSharedObject( $this );
        }
    }

    // -- methods

    protected function cachedDataHash()
    {
        return md5(
            serialize(
                $this->_cached_data instanceof \Core\Kernel\Model\Entity
                    || $this->_cached_data instanceof \Core\Kernel\Model\Collection
                ? $this->_cached_data->toArray()
                : $this->_cached_data
            )
        );
    }

    protected function cachedDataHasChanged()
    {
        return $this->_cache_hash != $this->cachedDataHash();
    }

}