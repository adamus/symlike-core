<?php
namespace Core\Kernel\Entity;

use Core\Kernel\Entity\SharedCache;
use Core\Kernel\Driver\CacheController;

class CachedSetup extends SharedCache
{
    protected $_maxcache = 600;

    function __construct()
    {
        $this->onCacheLimit();
    }

    function onCacheLimit()
    {
        $this->_cached = time();
        $this->_cached_data = new \Core\Kernel\Model\Collector('db_setup', array(1=>1), '\\stdClass', '', 'ORDER BY name');
        CacheController::putSharedObject( $this );
    }

}